# Load modules.
from pprint import pprint
import os, sys

# Change working directory.
os.chdir("/tmp")
print(os.path.abspath(os.curdir))

# Append custom module paths.
module_path = os.path.expanduser("~/Projects/GOSH/gosh-col-dev/pipettin-grbl/klipper/code/")
sys.path.append(module_path)
# Load piper modules.
from piper.commander_utils_mongo import MongoObjects
from piper.gcode import GcodeBuilder

# Setup database tools.
mo = MongoObjects(mongo_url='mongodb://localhost:27017/', verbose=False)

# List protocols (will print possible protocol names).
protocols = mo.listProtocols()

# Get the last protocol.
protocol = protocols[-1]
protocol_name = protocol["name"]

# Manually choose a protocol.
# protocol_name = "Mixing test 2023-02-05T22:59:49.376Z"
print(protocol_name)
pprint(protocol)

# Get everything about this protocol by its name.
protocol, workspace, platforms_in_workspace = mo.getProtocolObjects(protocol_name=protocol_name)

# Inspect the protocol actions.
actions = protocol["actions"]
pprint(actions)

# Example actions
example_actions = [
    {'cmd': 'HOME'},
    {'args': {'item': '200ul_tip_rack_MULTITOOL 1', 'tool': 'P200'},
    'cmd': 'PICK_TIP'},
    {'args': {'item': '5x16_1.5_rack 1',
            'selector': {'by': 'name', 'value': 'tube1'},
            'volume': 100},
    'cmd': 'LOAD_LIQUID'},
    {'args': {'item': '5x16_1.5_rack 1',
            'selector': {'by': 'name', 'value': 'tube2'},
            'volume': 100},
    'cmd': 'DROP_LIQUID'},
    {'args': {'item': 'descarte 1'}, 'cmd': 'DISCARD_TIP'},
    {'args': {'text': 'End step: step1'}, 'cmd': 'COMMENT'},
    {'cmd': 'HOME'}
]

# Instatiate the builder class, specifying tool parameters (pipettes).
builder = GcodeBuilder(workspace=workspace, platformsInWorkspace=platforms_in_workspace)

# Add a "home" action.
gcode1 = builder.addAction(action={'cmd': 'HOME'})
# Add a "pick tip" action.
gcode2 = builder.addAction(action={'args': {'item': '200ul_tip_rack_MULTITOOL 1', 'tool': 'P200'}, 'cmd': 'PICK_TIP'})

# Inspect the resulting GCODE commands.
pprint(gcode1)
pprint(gcode2)
