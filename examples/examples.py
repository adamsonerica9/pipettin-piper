# Load modules.
from pprint import pprint
import os, sys, time

# Change working directory.
os.chdir("/tmp")
print(os.path.abspath(os.curdir))

# Append custom module paths.
module_path = os.path.expanduser("~/Projects/GOSH/gosh-col-dev/pipettin-grbl/klipper/code/piper/")
sys.path.append(module_path)
# Load piper modules.
from piper.commander_utils_mongo import MongoObjects
from piper.gcode import GcodeBuilder

# Setup database tools.
mo = MongoObjects(mongo_url='mongodb://localhost:27017/', verbose=False)

# List protocols (will print possible protocol names).
protocols = mo.listProtocols()

# Get the last protocol.
protocol = protocols[-1]
protocol_name = protocol["name"]

# Manually choose a protocol.
# protocol_name = "Mixing test 2023-02-05T22:59:49.376Z"

# Get everything about this protocol by its name.
print(protocol_name)
pprint(protocol)
protocol, workspace, platforms_in_workspace = mo.getProtocolObjects(protocol_name=protocol_name)

# Inspect the protocol.
pprint(protocol)

# Instatiate the builder class.
builder = GcodeBuilder(protocol, workspace, platforms_in_workspace, verbose = True)

# Generate GCODE for the task, it is saved in the task class object.
builder.parseProtocol()

# Get the command list, and print it.
pprint(builder.commands)

# Write the gcode to a file
timestamp = time.ctime().replace(" ", "_")
builder.save_gcode(path=f'/tmp/{timestamp}-example.gcode')
