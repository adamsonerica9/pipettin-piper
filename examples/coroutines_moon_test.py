
def moon_test():
    commands = [
        {"jsonrpc": "2.0", "method": "printer.objects.list", "id": 5},
        {"jsonrpc": "2.0", "method": "printer.info", "id": 7}
    ]

    tracker = {}
    
    background_task = asyncio.create_task(
        moon_commander(commands, tracker,
                    spam_info=[True],
                    min_interval=1,
                    background=False)
    )
    
    commands.append("quit")

    reader_out, tracker_out = await background_task
    
    print(background_task.done())

    if not background_task.done():
        print(background_task.cancel())
        print(background_task.cancelling())

    time.sleep(1)
    print(background_task.cancelled())
