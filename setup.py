#!/usr/bin/env python
# See: https://stackoverflow.com/a/62983901/11524079
#      https://stackoverflow.com/a/65162874/11524079

import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

if __name__ == "__main__":
    setuptools.setup(
        name='piper',
        packages=['piper'],
        # packages=setuptools.find_packages()
        long_description=long_description,
        long_description_content_type="text/markdown",
        # https://python-packaging.readthedocs.io/en/latest/dependencies.html
        install_requires=[
            'pymongo', #'pymongo==3.4.0',  # Older version for RPi's old mongo "wire" version.
            'python-socketio', 'aiohttp', 'websockets'
        ],
        license='AGPL-3.0-or-later'
    )
