# Piper: GCODE generation and Klipper handlers for pipetting

> Work in progress!

This software is distributed under the terms of the GNU Affero GPLv3 license. See [LICENSE.txt](./LICENSE.txt).

## Klipper handler modules

These module handles commands from the GUI (through socketio) and forwards them to Moonraker (through websockets).

- [coroutines_moon.py](./coroutines_moon.py): main controller class. Communicates with Moonraker/Klipper through `websockets` and with the pipetting GUI through `SocketIO`.
- [commander_utils.py](./commander_utils.py): functions to parse arguments from GUI commands, and routing them to execute the corresponding implementation. Uses functions from [gcode_primitives.py](./gcode_primitives.py), requires the commander class from [coroutines_moon.py](./coroutines_moon.py), and the MongoDB utils class in [commander_utils_mongo.py](./commander_utils_mongo.py).
- [commander_utils_mongo.py](./commander_utils_mongo.py): MongoDB utils class, to load objects from the database.
- [rpc_primitives.py](./piper/rpc_primitives.py): helper functions to generate RPC commands for Moonraker.

Deprecated:

- [coroutines.py](./coroutines.py): asyncio functions to communicate with Moonraker. These have been migrated to a class in [coroutines_moon.py](./piper/coroutines_moon.py), but should work as boilerplate code.
- [klippy_run.py](./klippy_run.py): If klipper is not running, it can be started with the functions defined here. Currently we manage Klipper using systemd units.

### Usage

The module's main coroutines can now be launched in three ways.

1) From the REPL after installing:

```
$ python
>>> import piper
>>> moon_kontrol = piper.moonControl()
>>> moon_kontrol.start()
```

2) Launch piper from the CLI with installation from anywhere:

```
$ python3 -m piper.coroutines_moon
```

3) Launch piper from the CLI without installation from its source dir:

```
$ cd piper
$ python3 -m piper.coroutines_moon
```

More or less updated usage examples are available (read on).

Scripts with simple examples are here: [examples](./examples)

- [examples.py](./examples/examples.py): building GCODE from a pipetting protocol defined in the GUI/MongoDB.
- [examples_action_stream.py](./examples/examples_action_stream.py): example actions and building GCODE from them.
- [coroutines_moon_test.py](./examples/coroutines_moon_test.py): deprecated.

Notebook examples are here: [notebooks](../notebooks)

- [moonraker_websocket_test.ipynb](./notebooks/moonraker_websocket_test.ipynb): code snippets for connecting to Klipper through Moonraker (through wbesockets or HTTTP).
- [klipper_uds_test.ipynb](./notebooks/klipper_uds_test.ipynb): newer tests for connecting to Klipper through its Unix-domain socket (UDS).
- [klipper_uds_test-old.ipynb](./notebooks/klipper_uds_test-old.ipynb): older code snippets for connecting to Klipper through its Unix-domain socket (UDS).
- [examples_piper-development.ipynb](./notebooks/examples_piper-development.ipynb): many code snippets to connect to and/or run Klipper through different protocols (websockets, RPC, HTTP). Uses functions in [coroutines.py](./piper/coroutines.py) and [klippy_run.py](./piper/klippy_run.py). Contains most of the snippets in other notebooks.
- [examples_piper.ipynb](./notebooks/examples_piper.ipynb): initial tests of the asyncio coroutines in [coroutines.py](./piper/coroutines.py).

### Dry run

To run the module without sending commands to the robot, use the `dry` option.

The main module can be called from the CLI in a precarious but effective way.

```bash
python3 -m piper.coroutines_moon dry verbose
```

You can follow most  messages sent to logging by tailing the log file in a new terminal:

```bash
tail -f -n0 /tmp/moon.log
```

Hit `Ctrl+C` to stop any of those commands.

The messages can be useful to see what is going on or debug external software.

## Protocol to GCODE modules

> A protocol _slic3r_.

- [gcode_primitives.py](./gcode_primitives.py): functions to generate GCODE, used by [gcode.py](./gcode.py).
    - All GCODE is generated here.
    - Good place to adapt the module to a new CNC machine or Firmware.
- [gcode_utils.py](./gcode_utils.py): pipette data definitions.
- [gcode.py](./gcode.py): a class to translate high-level protocol actions to a GCODE. Calls functions in [gcode_primitives.py](./gcode_primitives.py) to generate GCODE.

_Legacy note_: the piper module replaces the older "protocol2gcode" module, which controlled GRBL (i.e. before we moved to Klipper).

## Dependencies

### Imports

Note that the version of the node `socket.io` package (installed for the GUI) must match the version of the installed Python3 `python-socketio` package. See:

- https://stackoverflow.com/questions/66809068/python-socketio-open-packet-not-returned-by-the-server
- https://python-socketio.readthedocs.io/en/latest/intro.html#version-compatibility
- For node's `socket.io` package version 2.5.0, run: `pip3 install python-socketio==4.6.1` 
- To list node packages: `npm list socket.io`
- To list python packages: `pip show python-socketio`

The installed MongoDB version must match the version of the Python3 `pymongo` package. See:

- https://www.mongodb.com/docs/ruby-driver/master/api/Mongo/Server/Description/Features.html
- https://www.mongodb.com/docs/drivers/pymongo/#compatibility
- For mongo version 3.6 and python version 3.7.2 this was solved with: `pip3 install pymongo==3.13.0` (the last version supporting mongo 3.2)
- To list python packages: `pip show pymongo`

### Klipper modules

Klipper modules (i.e. extensions) are python files placed in the "extra" directory of the `klippy` module. They are loaded when a correscponding entry is placed in a Klipper config file. There are currently two new modules needed for full functionality.

New modules and core mods here: https://github.com/naikymen/klipper-for-cnc

This Klipper fork is needed for the project, it implements "homing extruder" and "G38.n" general probing moves, and more.

## Plugins

A plugin system is in development, visit [plugins](./piper/plugins) for details.
