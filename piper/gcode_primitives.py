# https://github.com/jminardi/mecode
# from mecode import G
# g = G()

# def gcodeProbeZ(z_probe=12, x_probe=0, y_probe=0, z_scan=-2.5, probe_height=52, mode="G38.2"):
#     """
#     NO LONGER IN USE!!!
#     This macro was meant to callibrate the pipette's Z position using a "thingy" of known height on the workspace.
#     GRBL would probe the thing, stop, and overwrite the current Z position with the know height, using G92 (https://all3dp.com/2/g92-g-code-set-position/).
#     In this way, the coordinate system would have it's lowest point at "0", and would match the surface of the workspace.
    
#     For reference:
#     "G38.2" probe until probe is ON, error if probe doesnt activate during probing
#     "G38.3" probe until probe is ON, without error
#     """
#     return ["G90 G0 X{} Y{}".format(x_probe, y_probe),
#             "G90 G0 Z{}".format(str(z_probe+1)),
#             "G91 G0 Z-1",
#             "{} Z{} F200".format(mode, str(z_scan)),
#             "G92 Z{}".format(probe_height),
#             "G90 G0 Z{}".format(str(probe_height + 10))
#             ]

def comment(message, prefix=" ; ", suffix = ""):
    return prefix + str(message) + suffix

def gcodeHuman(text, human_prefix = "PAUSE"):
    human_command = [human_prefix + comment(text)]
    return human_command

def gcodeDwell(seconds, wait_prefix = "G4 P"):
    wait_command = [wait_prefix + str(seconds*1000)]
    return wait_command

def gcodeMove(x: float = None,
              y: float = None,
              z: float = None,
              e: float = None,
              f: float = None,
              _mode="G0",
              absolute=None):  # True for absolute (G90), False for relative (G91).
    """
    gcode to move robot to xyz and extruder to e, by pasting values of xyzsef if specified.
    @param absolute: Use G90/G91 for XYZ move (abs/rel). TODO: this should not affect E moves, M82/M83 check pending!
    """

    if (x is None) and (y is None) and (z is None) and (e is None):
        print("No values specified, returning empty string.")
        return [comment("No values passed to gcodeMove.")]

    command = [_mode]  # Default is G90 G0,

    # Coord parsing
    if x is not None:
        command.append(f"X{x}")
    if y is not None:
        command.append(f"Y{y}")
    if z is not None:
        command.append(f"Z{z}")
    if e is not None:
        command.append(f"E{e}")
    if f is not None:
        command.append(f"F{f}")

    # Build full move command
    full_command = " ".join(command)

    if absolute is None:
        return [full_command]
    else:
        # Motion coord mode
        coord_mode = gcodeCoordMode(absolute)
        # Build final command
        return [coord_mode[0], full_command]


def gcodeCoordMode(absolute=True):
    # Motion coord mode
    if absolute:
        coord_mode = "G90"
    else:
        coord_mode = "G91"

    return [coord_mode]

def gcodeCoordModeExtruder(absolute=True):
    # Motion coord mode
    if absolute:
        coord_mode = "M82"
    else:
        coord_mode = "M83"

    return [coord_mode]


def gcodeHomeXYZ(which_axis="all"):
    """
    Returns G28 for 'all' or either single axis homing commands for which_axis equal to x, y or z.

    Then sets absolute XYZ coordinates (G90), and relative E coordinates (M83).
    """
    
    # Default GRBL homing command
    home_command = "G28"

    # Append single axis specifier if required
    if which_axis != "all" and which_axis.upper().find("XYZ") == -1:
        if which_axis.upper().find("X") >= 0:
            home_command += " X"
        if which_axis.upper().find("Y") >= 0:
            home_command += " Y"
        if which_axis.upper().find("Z") >= 0:
            home_command += " Z"

    # For testing
    # home_command = "SET_ORIGIN X=0 Y=0 Z=0 E=0"
    
    return [home_command + comment("Homing command"),
            "G90" + comment("Absolute XYZ motion"),
            "M83" + comment("Relative E motion")]


def gcodeHomeP(which_axis="all", retraction_mm=None, cmd_prefix="H_T_", cmd_all="ALL", cmd_suffix=""):
    """
    Make homing moves for tools, using custom Klipper macro commands.
    """

    # Homing command prefix
    home_command = cmd_prefix

    if which_axis == "all":
        # A macro named "H_ALL" must be configured, which homes all tools in Klipper.
        home_command += cmd_all
    else:
        # Macros named "H_P200" (and similar) must be configured in Klipper.
        home_command += which_axis

    # Add the suffix
    home_command += cmd_suffix
    
    # Check for retract move
    if retraction_mm is None:
        home_command = [home_command + comment(f"Homed {which_axis} tool(s)")]
    else:
        # Ensure relative extruder motion
        home_command = [home_command + comment(f"Homed {which_axis} tool(s)")]
        home_command += [f"G0 E{str(retraction_mm)}" + comment("Tool homing retraction")]

    # For testing
    # home_command = ["SET_ORIGIN X=0 Y=0 Z=0 E=0"]

    return gcodeM83() + home_command

def gcodeM83():
    return ["M83" + comment("Ensure relative E motion")]

def gcodeG92(x: float = None,
             y: float = None,
             z: float = None):
    """Returns G92 setup command"""
    command = ["G92"]  # Default is G90 G0,

    if x is None and y is None and z is None:
        print("gcodeG92 warning: no values specified.")
        return [comment("gcodeG92 warning: no values specified in gcodeG92")]

    if x is not None:
        command.append("X" + str(x))

    if y is not None:
        command.append("Y" + str(y))

    if z is not None:
        command.append("Z" + str(z))

    return [" ".join(command)]


def probe_move(x=None, y=None, z=None, f=None,
               probe_towards=True, error_on_failure=False,
               command_prefix="G38"):
    """
    Example:

    import gcodePrimitivesKlipper as g
    g.probe_move(1,2,3)
    'G38.4 X1 Y2 Z3'
    
    For reference:
        - "G38.2" probe until probe is ON, error if probe does not activate during probing.
        - "G38.3" probe until probe is ON, without error.
    
    From LinuxCNC: https://linuxcnc.org/docs/2.6/html/gcode/gcode.html
        - G38.2 - (True/True) probe toward workpiece, stop on contact, signal error if failure.
        - G38.3 - (True/False) probe toward workpiece, stop on contact.
        - G38.4 - (False/True) probe away from workpiece, stop on loss of contact, signal error if failure.
        - G38.5 - (False/False) probe away from workpiece, stop on loss of contact.
        
    In the Klipper G38/probing module, this will use the probing endstop with a name matching the current tool.
    """

    if (x is None and y is None and z is None) or f is None:
        print("probe_move warning: insufficient values specified.")
        return [comment("probe_move warning: insufficient values specified in probe_move")]
    
    options = [[".2", ".3"], 
               [".4", ".5"]]

    # Get the "Xn Yn Zn" component of a GCODE move, removing the abs/rel command and the G0/G1 mode prefix.
    coords = " " + gcodeMove(x=x, y=y, z=z, f=f, _mode="")[0].strip()
    
    command = command_prefix + options[not probe_towards][not error_on_failure] + coords

    return [command]


def multi_probe_move(probe_name, f=10, x=None, y=None, z=None,
                     probe_towards=True, error_on_failure=False):
    """
    Klipper mux type command.
    
    Note that F is in mm/s units.
    """

    # Checks
    if (x is None and y is None and z is None):
        print("multi_probe_move warning: insufficient values specified.")
        return [comment("multi_probe_move warning: insufficient values specified in probe_move")]

    command_prefix = "MULTIPROBE"

    options = [["2", "3"],
               ["4", "5"]]

    command = command_prefix + options[not probe_towards][not error_on_failure] + f" PROBE_NAME={probe_name}"

    # Build coordinate arguments
    if x is not None:
        command += " X=" + str(x)
    if y is not None:
        command += " Y=" + str(y)
    if z is not None:
        command += " Z=" + str(z)
    # Add feedrate
    command += " F=" + str(f)

    return [command]


def gcodePipetteProbe(z_scan=-5, feedrate=10):
    """
    Movimiento para hacer "probing" al colocar el tip.
    
    In the Klipper G38/probing module, this will use the probing endstop with a name matching the current tool.
    
    Note that "feedrate" here is actually "speed" in mm/s units.
    """
    # Build command
    command = probe_move(z=z_scan, f=feedrate, probe_towards=True, error_on_failure=True)[0]
    # return f"{prefix}{mode} {axis}{z_scan} F{feedrate};probingtime_{abs(z_scan)/feedrate}"
    
    # Add probing time comment
    command += comment(f"Probe move with max probing time={abs(z_scan)/feedrate}")

    return ["G91" + comment("gcodePipetteProbe START"),
            command,
            "G90" + comment("gcodePipetteProbe END")]


def gcodeActivateTool(tool_name, prefix="", suffix=""):
    """
    Use the Klipper macros "P200" and "P20" to activate the corresponding tool.
    """
    return [prefix + tool_name.upper() + suffix + comment(f"Activating tool '{tool_name}'")]

def gcodeT(tool, prefix="", suffix=""):
    """Alias for gcodeActivateTool"""
    return gcodeActivateTool(tool, prefix, suffix)


def toolchange_probe(x, y, z, y_final, y_clearance, z_parking_offset, safe_z, safe_y,
                     probe_name,
                     extra_scan=1, closeup_dist=10, extra_undock_dist=0,
                     #mode_fwd="G38.2", mode_rev="G38.4"
                     feedrate=1000*60, safe_feedrate=600):
    """
    This will use regular GCODE and the spectial Klipper "MULTIPROBE" command from the probing module.
    Regular G38 cannot be used with the current implementation (it would require probing on two endstops simultaneously).
    
    First align to the parked tool. Then, do a probing scan, looking for the parking post's limit switch.
    When it activates, back out up to the initial location.
    NOTE: The $6=0 GRBL setting is required to use a mechanical end-stop as a probe.
    For reference:
    - "G38.2" probe until probe is ON, error if probe does not activate during probing.
    - "G38.3" probe until probe is ON, without error.
    TODO: conseguir informacion del probe position con "$#".
    Ver: https://github.com/gnea/grbl/wiki/Grbl-v1.1-Commands#---view-gcode-parameters
    
    From LinuxCNC: https://linuxcnc.org/docs/2.6/html/gcode/gcode.html
    
    - G38.2 - probe toward workpiece, stop on contact, signal error if failure.
    - G38.3 - probe toward workpiece, stop on contact.
    - G38.4 - probe away from workpiece, stop on loss of contact, signal error if failure.
    - G38.5 - probe away from workpiece, stop on loss of contact.

    @param x: Carriage coordinate at the front of the parking.
    @param y: Carriage coordinate at the front of the parking.
    @param z: Carriage coordinate at the front of the parking.
    @param y_final:
    @param y_clearance: Length of the tool along the Y axis when it is loaded, measured from the front of the carriage.
    @param z_parking_offset: Difference between the Z coordinate for parking, relative to the Z coordinate for loading.
    @param safe_z: General Z clearance from platforms in the workspace.
    @param safe_y: General Y clearance from other parked tools.
    @param probe_name: Klipper config name for the probe to use in the MULTIPROBE command.
    @param extra_scan: Tool-change probing extra scan distance, relative to the exact Y position for docking.
    @param closeup_dist: A fast move will be made between "y" and "y_final-closeup_dist", the rest of the distance to the probe will be done slowly.
    @param extra_undock_dist: After clicking the latch and probing back, Y axis will move to "y-extra_undock_dist", to ensure undocking from the toolpost.
    @param mode_fwd: Forward probing mode.
    @param mode_rev: Reverse probing mode.
    @param feedrate: General feedrate.
    @param safe_feedrate: Undocking and probing feedrate.
    @return:
    """

    # f"{mode_fwd} Y{y_final + extra_scan} F{feedrate}" + comment("probe the remaining Y distance."),
    probe_fwd = multi_probe_move(probe_name=probe_name, y=y_final + extra_scan, f=safe_feedrate/60,
                                 probe_towards=True, error_on_failure=True)
    # f"{mode_rev} Y{y} F{feedrate}" + comment("probe back."),
    probe_rev = multi_probe_move(probe_name=probe_name, y=y, f=safe_feedrate/60,
                                 probe_towards=False, error_on_failure=True)

    command = [comment("START tool-change macro."),
               comment("Safety clearance moves."),
               "G90" + comment("Set absolute motion mode."),
               f"G0 Z{safe_z} F{feedrate}" + comment("move to the safe Z height."),
               f"G0 Y{safe_y}" + comment("move to the Y coordinate clear of other parked tools."),
               comment("Alignment moves."),
               f"G0 X{x}" + comment("move to the X position in front of the parking post."),
               f"G0 Y{y} Z{z+z_parking_offset}" + comment("move to the Y position in front of the parking post, and align to the post's Z."),
               comment("Docking moves."),
               f"G0 Y{y_final - closeup_dist}" + comment("insert the alignment rods."),
               f"{probe_fwd[0]}" + comment("Probe the remaining Y distance."),
               f"{probe_rev[0]}" + comment("Probe back."),
               f"G0 Y{y-extra_undock_dist}  F{safe_feedrate}" + comment("back away from the parking area, undocking the tool from the post."),
               comment("Safety clearance moves."),
               f"G0 Y{y - y_clearance} F{feedrate}" + comment("back out of the way of other tools."),
               f"G0 Z{safe_z}" + comment("move to the safe Z height."),
               comment("END tool-change macro.")]
    return command


def gcodeEjectTip(safe_z, safe_y, post_x, post_y, post_z_pre, post_z, 
                  feedrate=1000*60, eject_feed_rate=600):
    """
    Tip ejection macro based on controlled crashing with a "post".

    @param safe_z: Clearance Z position ofr the current workspace and tool.
    @param safe_y: Clearance Y position ofr the current workspace and tool.
    @param post_x: X-axis coordinate of the carriage when the pipette's eject button is under the ejection post.
    @param post_y: Y-axis coordinate of the carriage when the pipette's eject button is under the ejection post.
    @param post_z_pre: Z-axis coordinate of the carriage when the pipette's eject button is under the ejection post, but not in contact with it.
    @param post_z: Z-axis coordinate of the carriage when the pipette's eject button is fully pressed the ejection post, and a tip is ejected.
    @param feed_rate:
    """

    # f"G1 G90 Z{post_z} F{feed_rate}; eject the tip",
    # probe_rev = probe_move(z=post_z, f=feed_rate,
    #                        probe_towards=False, error_on_failure=True)
    
    commands = [
        comment("START tip-ejection macro."),
        "G90" + comment("Set absolute motion mode."),
        f"G0 Z{safe_z} F{feedrate}" + comment("go to safe Z (workspace tip box)"),
        f"G0 Y{safe_y}" + comment("go to safe Y (parked tools)"),
        f"G0 X{post_x}" + comment("Align X position"),
        f"G0 Z{post_z_pre}" + comment("pre-approach Z position"),
        f"G0 Y{post_y}" + comment("approach the toolpost"),
        
        # BUG: The probe is unstable in the reverse direction. A regular mechanical endstop would be better.
        # probe_rev[0] + comment("eject the tip"),     
        # NOTE: Use a regular upwards move instead.
        f"G1 Z{post_z} F{eject_feed_rate}" + comment("eject the tip"),
        
        f"G0 Z{post_z_pre} F{feedrate}" + comment("relax"),
        f"G0 Y{safe_y}" + comment("move away from the ejection post up to safe Y (parked tools)"),
        # The safe_z distance will actually change during this GCODE.
        # Let the actionDISCARD_TIP function handle this.
        # f"G0 Z{safe_z}" + comment("go to safe Z (workspace tip box)")
        comment("END tip-ejection macro.")
    ]
    
    return commands
