# Third party imports.
import time
import asyncio
import aiohttp  # Imported even if unused explicitly, as it is required for HTTP requests by asyncio/websockets.
import json
from pprint import pprint, pformat
import traceback
import websockets
from hashlib import md5
import socketio
from copy import deepcopy
import sys
import importlib
import logging

# Internal imports
from .dummy import Dummy

class SioCommander(Dummy):
    
    # NOTE: I'm not sure about sync vs async clients, 
    #       but AsyncClient seems more flexible. The
    #       caveat is that it requires extra dependencies
    #       not installed (and not warned about) that do 
    #       not produce visible errors.
    #sio = socketio.Client()  # Available as self.sio?
    sio = socketio.AsyncClient()  # Available as self.sio?

    def __init__(self,
                 sio_address="http://localhost:3333",
                 plugins=("p2g_command",),
                 config={},
                 *args, 
                 **kwargs):

        # Save configuration.
        self.config = config  # NOTE: probably redundant, as is a Controller attribute already.

        # Add socketio coroutine to the methods list (created by KlipperCommander).
        self.coroutine_methods.append(self.moon_sio())

        # Register default socketio event handlers.
        self.sio_address = sio_address
        self.register_sio_callbacks()

        # Loop over the requrested plugins, which must be the names of
        # python files (without ".py" extension), and "load" them into a dictonay property.
        self.plugins={}
        # Load python modules from the 'plugins' directory through their 'load_listener' function.
        for module_name in plugins:
            # Load the requested module from the "plugins" sundirectory.
            module = importlib.import_module("piper.plugins." + module_name)
            # Save the module to the plugins list.
            self.plugins[module_name] = module.load_listener(controller=self)

        # Initialize the next parent of the 'Controller' class.
        super().__init__(*args, **kwargs)

    # SOCKET.IO CALLBACKS AND EVENTS SECTION ####
    def register_sio_callbacks(self):
        """Function to register socketio event callbacks, typically sent by the Pipettin GUI.
        
        In theory using this structure will allow callbacks to access the "self" object when they trigger.
        
        See:
        - https://github.com/miguelgrinberg/python-socketio/issues/390#issuecomment-787796871
        - https://clp-research.github.io/slurk/slurk_bots.html
        """

        @self.sio.event
        def connect():
            logging.info("Connected to the socket.io server.")

        @self.sio.event
        def disconnect():
            logging.info("Disconnected from the socket.io server.")

        @self.sio.on('*')
        def catch_all(self, event, data=None):
            logging.info(f"command_message_handler: received uncaugh event '{event}' socketio command with data: " + str(data))
            pass

    async def sio_emit_alert(self, text="No message specified.", type="alarm"):
        """Send 'alert' to the backend, the text would show up as a modal in the GUI.
        
        The type can be 'message', 'error', or 'alarm'.

        Example:
            data = {'text': 'text', 'type': 'error'}
        """
        if self.sio.connected:
            logging.debug(f"moon_sio: sending alert to socket: type={type} text={text}")
            # self.sio.emit('alert', {'text': text, 'type': type})
            self.sio.emit('alert', text)

    async def sio_emit_position(self, position):
        """Send 'position' to the 'tool_data' event.
        Example:
            data = {'position': {"x": 0, "y": 5, "z": 10}}
        """
        if self.sio.connected:
            logging.debug(f"moon_sio: sending position to socket: {pformat(position)}")
            self.sio.emit('tool_data', {'position': position})

    async def sio_emit_intervention(self, message):
        """Send 'message' to the 'human_intervention_required' event.
        Example:
            data = {'text': 'message'}
        """
        if self.sio.connected:
            logging.debug(f"moon_sio: sending human intervention message to socket: '{message}'")
            self.sio.emit('human_intervention_required', message)

    # MAIN COROUTINES method section ####
    # NOTE: Most coroutines were moved to the KlipperCommander.
    async def moon_sio(self, check_interval=2):
        logging.info("moon_sio: coroutine started.")

        try:
            if self.sio_address:
                while self.run:
                    if not self.sio.connected:
                        logging.warn("moon_sio: socketio disconnected.")
                        if self.sio_address is not None:
                            logging.debug(f"moon_sio: attempting reconnection to address: {self.sio_address}")
                            try:
                                await self.sio.connect(self.sio_address)
                                # await self.sio.emit('alert', {'text': "moon socket has reconnected.", 'type': 'message'})
                                await self.sio.emit('alert', "moon socket has reconnected.")
                                # await self.sio.wait()
                            except socketio.exceptions.ConnectionError as e:
                                logging.debug(f"Socket.io ConnectionError: '{e}'. Retrying in {check_interval} seconds.")
                        if not self.sio.connected:
                            logging.error(f"moon_sio: could not reconnect. Retrying in {check_interval} seconds.")
                    else:
                        logging.debug("moon_sio: connected.")
                    await asyncio.sleep(check_interval)

                # Disconnect after the while loop ends.
                if self.sio.connected:
                    try:
                        await self.sio.disconnect()
                    except Exception as e:
                        logging.error("moon_sio: Unhandled exception during sio.disconnect")
            else:
                logging.info("moon_sio: no socketio address configured.")
        
        except asyncio.exceptions.CancelledError:
            logging.error("moon_sio: asyncio task cancelled.")
        
        logging.info("moon_sio: coroutine ended.")

    async def close_socketio(self):
        # Close socketio
        if self.sio_address:
            if self.sio.connected:
                try:
                    await self.sio.disconnect()
                except Exception as e:
                    logging.error("close_socketio: Unhandled exception during self.sio.disconnect")
                    return False
        return True

    async def sio_alert(self, text):
        await self.sio.emit('alert', text)
    
    async def close(self, timeoout=3):
        # print("Closing SioCommander")
        # Close socketio
        await self.close_socketio()
        # Call the "close" method from the parent of the 'Controller' class.
        # Read: https://www.geeksforgeeks.org/multiple-inheritance-in-python/
        await super().close(timeoout)

    def status(self):
        # TODO: add printer status.
        status = {"sio_connected": self.sio.connected}

        # Let parent classes rport status as well.
        status.update(super().status())
        
        return status

    async def test(self):
        print("SioCommander test")
        await super().test()
        await asyncio.sleep(0.2)
