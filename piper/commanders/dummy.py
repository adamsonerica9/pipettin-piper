class Dummy:
    """
    This parent class must define 'terminal' methods, 
    and be the last parent for the 'Controller' class.
    This is helps other parent classes not worry about which
    one is "last" and just write 'super().test()' everywhere.
    """
    # Note that if one method is awaitable, all must be called in this way.
    sio = None
    async def test(self, *args, **kw):
        print("Dummy test")
    async def close(self, *args, **kw):
        print("Dummy close")
    def status(self, *args, **kw):
        return {}
