import time
import asyncio
import aiohttp  # Imported even if unused explicitly, as it is required for HTTP requests by asyncio/websockets.
import json
from pprint import pprint, pformat
import traceback
import websockets
from hashlib import md5
import socketio
from copy import deepcopy
import sys
import importlib
import logging

# Internal imports.
from ..commander_utils import command_router, NotReadyError, CommanderError
from .. import rpc_primitives
from .dummy import Dummy

class KlipperCommander(Dummy):
    """A class to drive the Pipettin robot.

    It's main function is to run in the background, maintain a socket.io connection with the GUI, and
    a connection to Moonraker (which in turn drives the CNC machine). It also has several convenience
    methods to send actions from "pipetting protocols".
    
    The SocketIO client communicates with the Pipetting GUI; a Node JS web-application for designing workspaces and protocols.
    
    This class can (re)connect to Moonraker, send it commands, check responses, and other stuff.
    The "python-websockets" library is used for this purpose.
    
    This class is started by the "start" method, wich is blocking. The method is meant to be used by a background process.
    Alternatively, try using "await Controller.moon_commander()" in an asyncio context.
    
    Returns:
        Controller: Instance of the class.
    """

    def __init__(self,
                 commands=[],
                 tracker={},
                 initial_cid=666,
                 cid_overwrite=False,
                 min_interval=0.2,
                 spam_info=True,
                 background_writer=False,
                 # Setting this to false by default prevents the coroutines from stopping on a shutdown when no protocol is runnning.
                 ensure_printer_ready=False,
                 # Must use "0.0.0.0" to avoid OSEerror with multiple exceptions being raised.
                 # https://github.com/python-websockets/websockets/issues/593
                 # https://bugs.python.org/issue29980
                 ws_address="ws://0.0.0.0:7125/websocket",
                 plugins=(), config={},
                 verbose=True, 
                 dry=False,
                 *args, **kwargs):

        # Save configuration.
        self.config = config  # NOTE: probably redundant, as is a Controller attribute already.

        self.logging = logging
        self.idle = False
        # self.queue_length = 0
        self.printer_state = None
        self.printer_ready = False
        self.ensure_printer_ready = ensure_printer_ready
        self.background = background_writer
        self.spam_info = spam_info
        self.min_interval = min_interval
        self.ws_address = ws_address
        self.cid = initial_cid
        self.cid_overwrite = cid_overwrite
        self.ready = False
        self.websocket = None
        self.commands = commands
        self.tracker = tracker
        self.msg_list = []
        self.new_info_response = False
        self.verbose = verbose
        
        # List of methods that should be started as coroutines/tasks.
        self.coroutine_methods.extend([ 
            self.moon_reconnect(),
            self.moon_reader(),
            self.moon_firmware_watchdog(),
            self.moon_writer(),
            self.moon_info()
        ])
        
        # Dry mode for testing the "p2g_command" event.
        self.dry = False
        if dry is not False:
            self.dry = True

        # Wether to close the websocket and clear the "run" flag when the moon_writer loop ends.
        self.cleanup_on_writer_end=False

        # Loop over the requrested plugins, which must be the names of
        # python files (without ".py" extension), and "load" them into a dictonay property.
        self.plugins={}
        # Load python modules from the 'plugins' directory through their 'load_klipper_plugin' function.
        for module_name in plugins:
            # Load the requested module from the "plugins" sundirectory.
            module = importlib.import_module("piper.plugins." + module_name)
            # Save the module to the plugins list.
            self.plugins[module_name] = module.load_klipper_plugin(self)

        # Initialize the next parent of the 'Controller' class.
        super().__init__(*args, **kwargs)


    # SOCKET.IO CALLBACKS AND EVENTS SECTION ####
    # NOTE: moved to "p2g_command" plugin.

    # CHECKS method section ####
    def check_tracker(self):
        """
        Check for "error" in the all responses of the tracker dict.
        """
        result = True
        for item in self.tracker:
            if "response" in item:
                if "error" in item["response"]:
                    # An error message was found in the response,
                    # update to False.
                    result = False
                else:
                    # Normal response.
                    # True unless updated in other iterations.
                    result = result
            else:
                # No response was found,
                # update to False.
                result = False

        return result

    def check_tracker_scripts(self, tracker=None):
        """
        Check for "not ok" responses in "script" (GCODE) entries of the tracker dict.
        
        Entries with no response (yet) are ignored.
        """

        if not tracker:
            tracker = self.tracker

        for item in tracker:
            # Skip the item if it does not match a GCODE command
            try:
                if item["command"]["method"] != 'printer.gcode.script':
                    continue
            except KeyError as e:
                continue
            
            # Look for a response and a result
            try:
                # Positive results can be "ok" or "ok withsomethingelsehere..."
                if not str(item["response"]["result"]).startswith("ok"):
                    # If the above key si found and is not "ok",
                    # then something went wrong.
                    return False

            except KeyError as e:
                # If the keys were not found,
                # The response or the result are missing,
                # and they should be waited for.
                return False
        
        # The function gets here if everything went well.
        return True

    async def check_command_result_ok(self, cmd_id, timeout=0.0, loop_delay=0.2):
        """
        Check for errors / "ok" in the response to a command with id=str(cmd_id)
        """
        elapsed = 0.0
        while True:

            # Check for an "ok" response
            try:
                response = self.tracker.get(str(cmd_id), {}).get("response", {})
                result = response.get("result", None)
                if str(result).startswith("ok"):
                    return True
                if response.get("error", None):
                    return False
            except (KeyError, AttributeError) as e:
                pass

            # Check for a timeout
            if timeout:
                if elapsed >= timeout:
                    logging.warn("check_command_result_ok: timed-out checking for response for cmd_id=" + str(cmd_id))
                    return None

            # Wait for a bit
            await asyncio.sleep(loop_delay)
            elapsed += loop_delay

    async def check_command_response(self, cmd_id, timeout=0.0, loop_delay=0.2):
        """
        Check for a response to a command with id=str(cmd_id)
        """
        elapsed = 0.0
        while True:

            # Check for a "response"
            if self.get_response_by_id(cmd_id):
                return True
            
            # This code executes if no response was found.
            if timeout:
                # Check for a timeout and return if it has elapsed.
                if elapsed >= timeout:
                    contents = pformat(self.get_command_by_id(cmd_id))
                    logging.warn(f"check_command_response: timed-out checking for response for cmd_id={cmd_id} and contents: {contents}")
                    return False
            else:
                # If no timeout was specified, then return immediately.
                logging.debug("check_command_response: no response yet for cmd_id=" + str(cmd_id))
                return False

            # Wait for a bit before checking again.
            await asyncio.sleep(loop_delay)
            elapsed += loop_delay

    def check_printer_ready(self, response, info_id_prefix="moon_info"):
        """
        Checks if a response contains a result->state equal to "ready".
        Raises an exception if self.ensure_printer_ready is True.
        Context: called by moon_reader when a response is received.
        """
        logging.debug("check_printer_ready: checking for " + info_id_prefix + " response.")
        try:
            if str(response["id"]).startswith(info_id_prefix):
                # Get state
                state = response["result"]["state"]
                # Update state.
                self.printer_state = state
                self.printer_ready = self.printer_state == 'ready'
                # Release "wait_for_info_update".
                self.new_info_response = True
                # Handle "not ready" state.
                if not self.printer_ready:
                    # Stop the coroutines if ready must be ensured
                    if self.ensure_printer_ready:
                        logging.debug("check_printer_ready: printer not ready, setting run=False due to ensure_printer_ready=True.")
                        self.run = False
                        # systemd.daemon.notify("STOPPING=1")
                    raise NotReadyError(f"check_printer_ready: state not 'ready' with state='{state}'.")
                else:
                    logging.debug("check_printer_ready: printer ready!")
            else:
                logging.debug(f"check_printer_ready: command with id={str(response['id'])} does not look like a status report with info_id_prefix={info_id_prefix}")
        except KeyError as e:
            logging.debug(f"check_printer_ready: could not check message due to KeyError: {e}")
        except NotReadyError as e:
            logging.debug(f"check_printer_ready: NotReadyError with message: {e}")
            
        return self.printer_state
            
    # WAIT method section ####
    async def wait_for_info_update(self, wait_time=None, timeout=4.0):
        """
        Wait for a new update from "check_printer_ready".
        """
        # This will be reset to "True" by "check_printer_ready" when a new status update arrives.
        self.new_info_response = False
        
        if wait_time is None:
            wait_time = self.min_interval
            
        logging.debug(f"wait_for_info_update: waiting with timeout={timeout} and interval={wait_time}")

        # Wait
        elapsed = 0.0
        while not self.new_info_response:
            await asyncio.sleep(wait_time)
            elapsed += wait_time
            if elapsed >= timeout:
                logging.debug("wait_for_info_update: Timed out.")
                return False

        logging.debug("wait_for_info_update: success!")
        return True

    async def wait_for_ready(self, reset=False, wait_time=1.1, timeout=8.0):
        """
        Wait until the printer becomes ready, optionally sending a "FIRMWARE_RESTART" command.
        
        This method is similar to "wait_for_idle_printer", but this one relies on regular status
        updates (not on object queries), and can restart klipper.
        """
        logging.debug("wait_for_ready: waiting...")
        
        # Track elapsed time
        initial_time = time.time()

        # Force wait for a new update.
        if not await self.wait_for_info_update(timeout=timeout):
            logging.debug("wait_for_ready: no new info received 1/2.")
        else:
            logging.debug("wait_for_ready: new info received 1/2.")
        # Else, new status info was received.

        # Reset printer if requested and not ready.
        if not self.printer_ready and reset:
            logging.debug("wait_for_ready: sending firmware_restart.")
            await self.firmware_restart()

        # Subtract the elapsed time from the timeout before running the next command.
        elapsed_time = time.time() - initial_time
        if elapsed_time >= timeout:
            timeout = 0.0
        else:
            timeout -= elapsed_time
        
        # Wait for an update again.
        if not await self.wait_for_info_update(timeout=timeout):
            # Break here if no new info was received.
            logging.debug("wait_for_ready: no new info received 2/2.")
            return False
        else:
            logging.debug("wait_for_ready: new info received 2/2.")

        # Wait here for printer ready.
        while not self.printer_ready:
            await asyncio.sleep(wait_time)
            timeout -= wait_time
            if self.printer_ready:
                # Break if the printer became ready
                break
            elif timeout <= 0:
                # If not ready, and the timeout is over, return False.
                logging.debug("wait_for_ready: Timed out.")
                return False

        # Ready! this part of the code is only reached then the printer is ready.
        assert(self.printer_ready)  # Check again just in case.
        logging.debug("wait_for_ready: success!")
        return True

    async def wait_for_setup(self, timeout=5.0, wait_time=None, raise_error=False):
        """Wait for the Moonraker connection to be established."""

        # Track elapsed time
        initial_time = time.time()
        if not wait_time:
            wait_time = self.min_interval

        # Wait here for websocket ready.
        while not self.ready:
            if timeout <= 0:
                # If not ready, and the timeout is over, return False.
                logging.debug("wait_for_ready: Timed out.")
                return False
            await asyncio.sleep(wait_time)
            timeout -= wait_time

        # Raise an error on timeout if requested.
        if raise_error and not self.ready:
            raise Exception("Timed-out waiting for the connection to moonraker to be established.")

        return self.ready

    # MISC utilities method section ####
    def get_result_by_id(self, cmd_id):
        """
        Lookup a response's "result", retuning it, or None if not found.
        """
        response = self.get_response_by_id(cmd_id)
        if response:
            try:
                return response["result"]
            except KeyError as e:
                logging.warn(f"get_result_by_id: failed to find result with cmd_id={cmd_id}")
    
        return None

    def get_response_by_id(self, cmd_id):
        """
        Lookup a "response", retuning it, or None if not found.
        """
        try:
            return self.tracker[str(cmd_id)]["response"]
        except KeyError as e:
            logging.warn(f"get_response_by_id: failed to find response with cmd_id={cmd_id}")
        
        return None

    def get_command_by_id(self, id):
        return [c for c in self.commands if c["id"] == id]

    # MAIN COROUTINES method section ####
    async def moon_firmware_watchdog(self, min_wait_for_restart=3.0):
        """
        Firmware restart coroutine.
        
        Will send a "FIRMWARE_RESTART" command if the printer is found in a "shutdown" state.
        """
        logging.info("moon_firmware_watchdog: coroutine started.")
        
        while not self.ready:
            await asyncio.sleep(self.min_interval)
        
        logging.info("moon_firmware_watchdog: ready.")
        recovering = False
        
        try:
            while self.run:
                # Wait for a new info update.
                if await self.wait_for_info_update():
                    # If shutdown, send a firmware restart.
                    if self.printer_state in ['shutdown', 'error']:
                        logging.warn(f"moon_firmware_watchdog: {self.printer_state} state found, restarting firmware and waiting for {min_wait_for_restart} seconds.")
                        # self.printer_state = "watchdog_restar_after_shutdown"
                        recovering = True
                        await self.firmware_restart()
                    elif recovering and self.printer_state != 'startup':
                        logging.info(f"moon_firmware_watchdog: recovery successful, printer state: {self.printer_state}")
                        recovering = False
                else:
                    logging.warn(f"moon_firmware_watchdog: wait_for_info_update timeout, retrying in {min_wait_for_restart} seconds.")
                # Wait for a bit
                await asyncio.sleep(min_wait_for_restart)
        
        except asyncio.exceptions.CancelledError:
            logging.error("moon_firmware_watchdog: coroutine cancelled.")

        except Exception as e:
            msg = "Unhandled exception at moon_firmware_watchdog: " + str(e)
            logging.error(msg)
            print(msg)
            traceback.print_exc()
            raise CommanderError(msg)
            
        logging.info("moon_firmware_watchdog: coroutine ended.")
    
    async def moon_reader(self):
        """
        The "reader" co-routine.
        This asyncio co-routine reads from the websocket continuously.
        The "read" is awaited, which means that other co-routines
        can still run while the reader is checking for messages.
        Note: a "co-routine" is similar to the old "serial_reader" thread, made with in Threading.
        """
        logging.info("moon_reader: coroutine started.")
        
        while not self.ready:
            await asyncio.sleep(self.min_interval)
        
        logging.info("moon_reader: coroutine ready.")
        
        try:
            while self.run:
                data = await self.websocket.recv()
                response = json.loads(data)
                self.msg_list.append(response)

                # Skip "notify_proc_stat_update" messages.
                if response.get('method', None) == 'notify_proc_stat_update':
                    logging.debug("moon_reader: received notify_proc_stat_update, continuing.")
                    continue
                
                # try:
                #     method = self.tracker[str(response["id"])]['command']['method']
                #     # if method == 'printer.info':
                # except KeyError:
                #     pass

                logging.debug(f"moon_reader: received response:\n{pformat(response)}")

                # if 'result' in response or 'method' in response:
                #     ...
                # else:
                #     logging.debug("moon_reader: no 'result' in response, message ignored.")

                # Add key to tracker dict
                try:
                    # Get the response ID
                    response_id = str(response["id"])

                    # Create an entry in the tracker if not present.
                    if response_id not in self.tracker:
                        self.tracker[response_id] = {}
                    
                    # Get the corresponding tracker entry
                    tracker_entry = self.tracker[response_id]
                    
                    # Look for an existing response.
                    if "response" in tracker_entry:
                        logging.debug(f"moon_reader warning: response found in tracker item with id={response_id} (it will be updated/overwritten).")
                        tracker_entry.update({"warning": "overwritten"})
                    
                    # Update tracker dict, saving the response in a "response" key.
                    tracker_entry.update({"status": "responded", "response": response})
                    logging.debug(f"moon_reader: item updated in tracker with ID {response_id}")
                    
                except KeyError as e:
                    logging.debug(f"moon_reader: the response might not contain an 'id' field. \nException message: {e} \nTraceback:" + traceback.format_exc())
                    continue

                # Process important messages (may raise an exception, or stop the coroutines, see definition).
                self.check_printer_ready(response)
                
                
                await asyncio.sleep(self.min_interval)

        except asyncio.exceptions.CancelledError:
            logging.error("moon_reader: task cancelled.")
        
        except Exception as e:
            msg = "Unhandled exception at moon_reader: " + str(e)
            logging.error(msg)
            print(msg)
            traceback.print_exc()
            raise CommanderError(msg)

        # except websockets.exceptions.ConnectionClosedOK as e:
        #     print("ConnectionClosedOK exception at moon_reader: " + str(e))
        #     traceback.print_exc()
        # 
        # except Exception as e:
        #     print("Unhandled exception at moon_reader: " + str(e))
        #     traceback.print_exc()
            
        logging.info("moon_reader: coroutine ended.")
        return self.msg_list

    async def moon_info(self, info_id_prefix="moon_info"):
        """
        Info request coroutine.
        
        Periodically sends "printer.info" requests to Moonraker.
        """
        logging.info("moon_info: coroutine started.")
        
        while not self.ready:
            await asyncio.sleep(self.min_interval)
        
        logging.info("moon_info: coroutine ready.")
        
        try:
            while self.run:
                # Check spam flag or wait and skip iteration
                if not self.spam_info:
                    await asyncio.sleep(self.min_interval)
                    continue

                # Increment ID counter
                cid = int(self.cid)
                self.cid = self.cid + 1
                
                # Build command:
                # info_command_id = f"{info_id_prefix}_{str(cid)}"
                info_command_id = f"{info_id_prefix}_{cid}_{time.time()}"

                # Generate "{"jsonrpc": "2.0", "method": "printer.info", "id": info_command_id}"
                # info_command = {"jsonrpc": "2.0", "method": "printer.info", "id": 7}
                info_command = rpc_primitives.printer_info(info_command_id)
                
                # Track time
                last_time = time.time()
                
                # Send command
                logging.debug("moon_info: sending info command: " + str(info_command))
                self.tracker[str(info_command["id"])] = {}
                self.tracker[str(info_command["id"])].update({"status": "sending", "command": info_command})
                
                data = json.dumps(info_command)
                await self.websocket.send(data)
                self.tracker[str(info_command["id"])].update({"status": "sent"})

                # Wait for the actual remaining time (after the previous await)
                new_time = time.time()
                if new_time - last_time <= self.min_interval:
                    await asyncio.sleep(self.min_interval - (new_time - last_time))

        except asyncio.exceptions.CancelledError:
            logging.error("moon_info: task cancelled.")
            
        except Exception as e:
            msg = "Unhandled exception at moon_info: " + str(e)
            logging.error(msg)
            print(msg)
            traceback.print_exc()
            raise CommanderError(msg)

        logging.info("moon_info: coroutine ended.")

    async def moon_writer(self):
        """
        The "writer" co-routine.
        This asyncio co-routine writes commands from a list into the websocket.
        There are two "awaits" here, that allow the serial reader to check for messages
        while this is running. First, with the "send" await, and then with a 
        1 second "sleep" await. Any of those can cause a *handoff* to the
        reader co-routine.
        Note: a "co-routine" is similar to the old "serial_reader" thread, made with in Threading.
        """
        logging.info("moon_writer: coroutine started.")
        
        while not self.ready:
            await asyncio.sleep(self.min_interval)
        
        logging.info("moon_writer: coroutine ready.")
        try:
            # Main writer loop
            while self.run and len(self.commands) > 0:
                # Send commands only if idle and ready, else only sleep.
                if self.idle and self.ready:
                    # Set busy flag.
                    self.idle = False
                
                    # Grab a command and delete it from the list
                    command = self.commands[0]
                    del(self.commands[0])
                    
                    # Increment the ID counter
                    id = int(self.cid)
                    self.cid = self.cid + 1

                    # Add the ID to the command if it is missing
                    if "id" not in command:
                        command["id"] = str(id)
                    
                    # Overwrite the command's ID if requested
                    if self.cid_overwrite:
                        # Backup
                        command["old_id"] = command["id"]
                        # And overwrite
                        command["id"] = str(id)

                    # Add key to tracker dict
                    if str(command["id"]) not in self.tracker:
                        self.tracker[str(command["id"])] = {}
                    else:
                        logging.debug("moon_writer: Warning, command found in tracker: " + str(command) + "\nIt will be overwritten.")
                        self.tracker[str(command["id"])].update({"warning": "overwriten"})
                    
                    # Update command tracker status
                    self.tracker[str(command["id"])].update({"status": "sending", "command": command})
                    
                    # Track time
                    last_time = time.time()
                    
                    # Send command
                    logging.info(f"moon_writer: Sending command: {pformat(command)}")
                    data = json.dumps(command)
                    await self.websocket.send(data)
                    self.tracker[str(command["id"])].update({"status": "sent"})
                    
                    data = json.dumps(command)
                    await self.websocket.send(data)
                    self.tracker[str(command["id"])].update({"status": "sent"})
                
                # Wait for the actual remaining time (after the previous await)
                new_time = time.time()
                if new_time - last_time <= self.min_interval:
                    await asyncio.sleep(self.min_interval - (new_time - last_time))
            
            # Flag idle.
            self.idle = True
            logging.info("moon_writer command list completed")
            
            # Secondary writer loop, responding to "appends" to the commands list,
            # only runs if "background" was set to True during init.
            if self.run and self.background:
                logging.info("moon_writer background loop started")
                # self.queue_length = len(self.commands)
            
            # Loop while active
            while self.run and self.background:
                # Track time
                last_time = time.time()
                
                # Check if a new command is available
                # Check for idle before sending commands, else retry at the next iteration.
                if len(self.commands) > 0 and self.idle and self.ready:
                    self.idle = False

                    command = self.commands[0]
                    del(self.commands[0])
                    
                    # Quit condition
                    if command == "quit":
                        logging.warn("moon_writer: Quit command received")
                        self.run = False
                        # systemd.daemon.notify("STOPPING=1")
                        break

                    # Increment the ID counter
                    cid = int(self.cid)
                    self.cid = self.cid + 1

                    # Add if if missing
                    if "id" not in command:
                        command["id"] = str(cid)
                    
                    # Overwrite the command's ID if requested
                    if self.cid_overwrite:
                        # Backup
                        command["old_id"] = command["id"]
                        # And overwrite
                        command["id"] = str(cid)

                    # Add key to tracker dict
                    if str(command["id"]) not in self.tracker:
                        self.tracker[str(command["id"])] = {}
                    else:
                        logging.debug("moon_writer: Warning, command found in tracker: " + str(command) + "\nWill overwrite.")
                        self.tracker[str(command["id"])].update({"warning": "overwriten"})
                    
                    # Update command tracker status
                    self.tracker[str(command["id"])].update({"status": "sending", "command": command})
                    
                    # Send command
                    logging.info(f"moon_writer: Sending new command:{pformat(command)}")
                    
                    data = json.dumps(command)
                    await self.websocket.send(data)
                    self.tracker[str(command["id"])].update({"status": "sent"})
                    
                    # Flag idle.
                    self.idle = True
                
                # Wait for the actual remaining time (after the previous await)
                new_time = time.time()
                if new_time - last_time <= self.min_interval:
                    await asyncio.sleep(self.min_interval - (new_time - last_time))
            
            logging.info("moon_writer secondary writer completed")
                
        except asyncio.exceptions.CancelledError:
            logging.error("moon_writer: task cancelled.")

        except Exception as e:
            logging.error("moon_writer: Unhandled exception: " + str(e))
            traceback.print_exc()

        # Signal the websocket to close, interrupting websocket readers.
        if self.cleanup_on_writer_end:
            logging.info("moon_writer: stopping coroutines and closing sockets.")
            # systemd.daemon.notify("STOPPING=1")
            await self.stop()

        logging.info("moon_writer: coroutine ended.")
    
    async def moon_reconnect(self, ws=None, wait_time=None, ws_open_timeout=3.0):
        """
        This method handles reconnection on disconnect.

        TODO: try to catch the error when no websocket is available:
            OSError: Multiple exceptions: [Errno 111] Connect call failed ('::1', 7125, 0, 0), [Errno 111] Connect call failed ('127.0.0.1', 7125)
        """
        logging.info("moon_reconnect started")
        if not wait_time:
            wait_time = self.min_interval*5
        
        # Try getting the classes websocket if it was not specified.
        if not ws:
            ws = self.websocket
        
        try:
            while self.run and self.ws_address:
                try:
                    # Create or re-create the connection
                    if ws is None:
                        logging.info("moon_reconnect: creating websocket")
                        ws = await websockets.connect(uri=self.ws_address, open_timeout=ws_open_timeout)
                    elif ws.closed and ws.open:
                        # Be aware that both open and closed are False during the opening and closing sequences.
                        logging.debug("moon_reconnect: websocket openclosed, waiting.")
                        pass # Wait until the next loop.
                    elif ws.closed:
                        logging.warn("moon_reconnect: websocket closed, recreating.")
                        ws = await websockets.connect(uri=self.ws_address, open_timeout=ws_open_timeout)
                    
                    if ws.open:
                        self.websocket = ws

                        logging.debug("moon_reconnect: websocket open, awaiting pong.")
                        pong = await ws.ping()
                        latency = await pong
                        logging.debug(f"moon_reconnect: connection latency={latency} seconds")
                        logging.debug(f"moon_reconnect: websocket.closed={ws.closed}")
                        logging.debug(f"moon_reconnect: websocket.open={ws.open}")
                        
                        # Signal start
                        # systemd.daemon.notify('READY=1')
                        self.ready = True

                    await asyncio.sleep(wait_time)

                except ConnectionRefusedError as e:
                    msg = f"moon_reconnect: ConnectionRefusedError error: '{e}'"
                    logging.error(msg)
                    print(msg)
                    traceback.print_exc()
                    msg = f"moon_reconnect: waiting for a bit before retrying."
                    await asyncio.sleep(3)

        except asyncio.exceptions.CancelledError as e:
            msg =f"moon_reconnect: got CancelledError, coroutine cancelled with message: '{e}'"
            logging.error(msg)
            print(msg)

        except Exception as e:
            msg = f"moon_reconnect: Unhandled exception: '{e}'"
            logging.error(msg)
            print(msg)
            traceback.print_exc()
            raise CommanderError(msg)
            # Terminate all coroutines
            # self.ready = False
            # self.run = False
            # systemd.daemon.notify('STOPPING=1')
        
        # except (websockets.ConnectionClosed, 
        #         websockets.exceptions.ConnectionClosedError, 
        #         websockets.exceptions.ConnectionClosedOK) as e:
        #     print(f"moon_reconnect: ConnectionClosed exception, reconnecting in {wait_time} seconds. Exception message: {e}")
        #     traceback.print_exc()
            
        #     # Signal websocket disconnect.
        #     # Will attempt to reconnect in the next for loop.
        #     # systemd.daemon.notify('RELOADING=1')
        #     self.ready = False
        
        logging.info("moon_reconnect: loop ended")

    # NOTE: moved moon_sio to Controller.

    # STOP/RESTART methods section ####
    async def emergency_stop(self):
        """
        See: https://moonraker.readthedocs.io/en/latest/web_api/#emergency-stop
        """
        e_stop = rpc_primitives.emergency_stop(id=42)
        data = json.dumps(e_stop)
        await self.websocket.send(data)

    async def firmware_restart(self):
        """
        See: https://moonraker.readthedocs.io/en/latest/web_api/#firmware-restart
        """
        fw_restart = rpc_primitives.firmware_restart(id=8463)
        data = json.dumps(fw_restart)
        await self.websocket.send(data)

    async def close_websocket(self, wait=True):
        """Utility function to close the class' websocket to Moonraker, and wait for that to happen."""
        if self.ws_address and self.websocket:
            try:
                if self.websocket.open:
                    await self.websocket.close()                # Close the websocket (interrupts recv).
                    if wait:
                        await self.websocket.wait_closed()          # And wait for it.
            except  Exception as e:
                logging.error("close: Exception at moon_commander while closing the websocket: " + str(e))
                print("close: Exception at moon_commander while closing the websocket: " + str(e))
                traceback.print_exc()
                return False
        
        return True

    async def close(self, timeoout=3):
        # print("Closing KlipperCommander")
        # Close websocket
        await self.close_websocket(wait=True)
        # Call the "close" method from the parent of the 'Controller' class.
        # Read: https://www.geeksforgeeks.org/multiple-inheritance-in-python/
        await super().close(timeoout)

    def status(self):
        # TODO: add printer status.
        status = {"printer_ready": self.printer_ready}

        # Let parent classes rport status as well.
        status.update(super().status())
        
        return status

    # GCODE methods section ####
    def make_gcode_cmd(self, gcode_cmd, cmd_id=None):
        """Create RPC compatible command from GCODE.
        
        Returns the RPC command (a dictionary) and the command ID."""
        # Create a cmd_id from md5sum (requires hashlib)
        if cmd_id is None:
            cmd_id = self.hash_cmd(gcode_cmd)

        # Build and send command
        command = rpc_primitives.gcode_script(gcode_cmd, cmd_id)

        return command, cmd_id

    @staticmethod
    def hash_cmd(gcode_cmd: str):
        # Should be unique by using the current time
        uid = gcode_cmd + str(time.time())
        uid_md5 = md5(uid.encode())
        cmd_id = uid_md5.hexdigest()
        return cmd_id

    async def send_gcode_cmd(self, gcode_cmd, cmd_id=None, wait=False, check=False, timeout=0.0):
        """
        Make RPC command from the GCODE, send it, and optionally check/wait for it.
        It wraps "send_cmd" for GCODE. This mechanism is independent of the 'writer' coroutine,
        and will not wait for idle.
        """

        # Build RPC command
        rpc_command, cmd_id = self.make_gcode_cmd(gcode_cmd=gcode_cmd, cmd_id=cmd_id)

        # Send the command
        logging.info(f"send_gcode_cmd: sending rpc_command={rpc_command}")
        cmd_id, checks = await self.send_cmd(rpc_cmd=rpc_command, cmd_id=cmd_id, wait=wait, check=check, timeout=timeout)

        return cmd_id

    async def send_gcode_script(self, gcode_cmds: list, cmd_id=None, wait=False, check=False, timeout=0.0):
        """Run a list of GCODES as a single script in Klipper. Hopefully will prevent some stuttering.

        Args:
            gcode_cmds (list): A list of strings, each a valid GCODE command for Klipper.

        Returns:
            str: tracking ID for the script command.
        """
        
        # Join multiple GCODE commands with a newline character. Klipper will eventually parse 
        # this with run_script (at "gcode.py") and split it back into the individual commands.
        gcode_script = "\n".join(gcode_cmds)

        # Reuse "send_gcode_cmd"
        logging.info(f"send_gcode_script: sending gcode_script={repr(gcode_script)}")
        cmd_id = await self.send_gcode_cmd(gcode_cmd=gcode_script, cmd_id=cmd_id, wait=wait, check=check, timeout=timeout)
        return cmd_id

    async def send_cmd(self, rpc_cmd, cmd_id=None, wait=False, check=False, timeout=0.0):
        """Send RPC command, and optionally check/wait for it.
        This mechanism is independent of the 'writer' coroutine, and will not wait for idle.
        """

        if self.killed():
            raise Exception("send_cmd: command received but the commander is not active.")

        # Generate unique command ID if an ID was not provided.
        if cmd_id is None:
            # Get the existing ID in the command or hash it.
            cmd_id = rpc_cmd.get("id", self.hash_cmd(str(rpc_cmd)))
        
        
        # Add command to the tracker.
        # Add an entry for the command if not there.
        self.tracker.setdefault(cmd_id, {})
        # Set the command, potentially overwriting the previous one (if cmd_id was specified and not unique).
        self.tracker[cmd_id]["command"] = rpc_cmd

        # Send the command
        logging.info(f"send_cmd: sending rpc_cmd={rpc_cmd}")
        # logging.debug("Sending RPC command: " + pformat(rpc_cmd))
        data = json.dumps(rpc_cmd)
        await self.websocket.send(data)
        
        # TODO: consider raising exceptions here.
        # Check results are "True" by default when not requested.
        wait_pass, check_pass = not wait, not check
        # Wait for an RPC response to arrive.
        if wait:
            wait_check = await self.check_command_response(cmd_id=cmd_id, timeout=timeout, loop_delay=0.2)
            if not wait_check:
                logging.error(f"send_cmd: failed to wait for a response for cmd_id={cmd_id}")
                wait_pass = False
            else:
                wait_pass = True
        # Run an "ok" check on the response.
        if check:
            ok_check = await self.check_command_result_ok(cmd_id=cmd_id, timeout=timeout, loop_delay=0.2)
            if not ok_check:
                logging.error(f"send_cmd: failed to check the response for cmd_id={cmd_id}")
                check_pass = False
            else:
                check_pass = True
        # Checks vector
        checks = tuple([wait_pass, check_pass])

        return cmd_id, checks

    def gcode_send_set_origin(self):
        """
        Set origin and unlock the machine. Relies on SET_KINEMATIC_POSITION.
        This mechanism is independent of the 'writer' coroutine, and will not wait for idle.
        """
        self.send_gcode_cmd(gcode_cmd="SET_KINEMATIC_POSITION X=0 Y=0 Z=0")

    def gcode_send_dwell(self, delay_milliseconds=0.1):
        """
        Dwell command. See: https://www.klipper3d.org/G-Codes.html#g-code-commands
        This mechanism is independent of the 'writer' coroutine, and will not wait for idle.
        """
        self.send_gcode_cmd(gcode_cmd=f"G4 P{str(delay_milliseconds)}")

    def gcode_send_wait(self):
        """
        Wait for current moves to finish: Add ability to fully stall the input until all moves are complete.
        See: https://github.com/Klipper3d/klipper/commit/2e03d84755f466adaad64ae0054eb461869d0529
        This mechanism is independent of the 'writer' coroutine, and will not wait for idle.
        """
        self.send_gcode_cmd(gcode_cmd="M400")

    async def wait_for_idle(self, timeout=10.0, loop_delay=0.2):
        """Wait until the "background" loop in the writer coroutine signals idle (no more commands due).
        The self.idle attribute is used to sync the 'writer coroutine' with the other protocol sending
        methods in this class (essentially 'run_rpc_protocol').
        """
        # Track start time
        start_time = time.time()
        elapsed = 0.0
        logging.debug(f"wait_for_idle: waiting for idle class status: start time {start_time} and timeout {timeout}")
        while True:
            # Check for writer idle
            if self.idle:
                logging.debug(f"wait_for_idle: success! start time={start_time} timeout={timeout}")
                return True
            # Check for a timeout
            if timeout:
                if elapsed >= timeout:
                    logging.debug(f"wait_for_idle: timed-out waiting for idle class status: start time={start_time} timeout={timeout}")
                    return False
            # Wait
            await asyncio.sleep(loop_delay)
            elapsed += loop_delay
    
    async def wait_for_idle_printer(self, timeout=10.0, loop_delay=0.5, idle_states = ["Ready", "Idle"]):
        """The 'idle_timeout' object reports the idle state of the printer.
        AFAIK, "Idle" is set by klipper (klippy/extras/idle_timeout.py) when the "idle timeout" is triggered successfully.
        
        Else, the printer may be idling and report a "Ready" state, or in a busy state, and report a "Printing" state.

        This method is similar to "wait_for_ready", except it will not restart Klipper (it only does queries to "idle").
        It is only meant to wait for a "still" printer which is running normally.

        The "response" to the query can look like this:
        
            {'id': 'aa3ba74fd3e3061ca58075924edec8f1',
            'jsonrpc': '2.0',
            'result': {'eventtime': 62094.128874252,
                        'status': {'idle_timeout': {'printing_time': 5.842246050997346,
                                                    'state': 'Printing'}}}}
        """

        # Track start time
        start_time = time.time()

        # Make query command
        idle_query_cmd = rpc_primitives.query_idle_timeout(id=None)
        elapsed = 0.0
        logging.debug(f"wait_for_idle_printer: waiting for idle printer with start_time={start_time}")
        while True:
            # Reset the commands unique ID.
            idle_query_cmd["id"] = self.hash_cmd(str(idle_query_cmd))

            # Track iter time
            last_time = time.time()

            try:
                # Send the command andwait for a response.
                logging.debug(f"wait_for_idle_printer: sending query command idle_query_cmd={idle_query_cmd}")
                cmd_id, checks = await self.send_cmd(rpc_cmd=idle_query_cmd, wait=True, check=False, timeout=timeout)
                # Get the printer's status from the response.
                cmd_result = self.get_result_by_id(cmd_id)
                status = cmd_result['status']['idle_timeout']["state"]
                logging.debug(f"wait_for_idle_printer: received response with status: {status}")
                # Check the response's result for printer idle.
                if status in idle_states:
                    logging.debug("wait_for_idle_printer: done.")
                    return True
            except KeyError as e: 
                logging.debug(f"wait_for_idle_printer: key error during response check with message: {e}")
            
            # Check for a timeout (total elapsed time, all loops).
            if timeout:
                elapsed = time.time() - start_time
                if elapsed >= timeout:
                    logging.debug("wait_for_idle_printer: Timed out.")
                    return False

            # Wait for the actual remaining time (after the previous loop)
            elapsed_time =  time.time() - last_time
            logging.debug(f"wait_for_idle_printer: Elapsed time: {elapsed_time}")
            if elapsed_time < loop_delay:
                logging.debug(f"wait_for_idle_printer: waiting for {loop_delay - elapsed_time} seconds.")
                await asyncio.sleep(loop_delay - elapsed_time)

    async def run_gcode_actions(self, actions):
        """Parses actions from a pipetting protocol, which already contain the corresponding GCODE, and executes them.

        Args:
            actions (List): A list of dictionaries, each one an action from a pipetting protocol.

        Returns:
            List: The same actions from the input, with extra information from runtime.
        """
        
        for action in actions:
            # Get GCODE commands
            gcode_commands = action["GCODE"]
            
            # Generate RPC commands from GCODE commands
            # NOTE: Only the dictionary is kept, and it's ID is discarded (hence the "[0]").
            rpc_commands = [self.make_gcode_cmd(cmd)[0] for cmd in gcode_commands]
            
            # Save RPC commands to the action dict.
            action["rpc_commands"] = deepcopy(rpc_commands)
            
            # Send RPC commands
            result = await self.run_rpc_protocol(commands=rpc_commands, check=True)
            
            # Save tracker (which holds responses and results)
            for rpc_command in rpc_commands:
                cmd_id = rpc_command["id"]
                try:
                    action["tracker"] = self.tracker[str(cmd_id)]
                except KeyError as e:
                    action["tracker"] = None
            
            # Log the return status and break the loop if an error occured.
            if result:
                action["status"] = "ok"
            else:
                action["status"] = "error"
                break
        
        return actions

    async def run_gcode_protocol(self, gcode_commands: list, wait=True, check=True, timeout=10.0):
        """Make RPC commands from a list of GCODE commands, and send it through the background writer loop.
        
        Optionally wait for (and/or check) the return status of each command. True by default, as a protocol should stop on failures.

        Args:
            gcode_commands (list): A list of GCODE commands (as strings).
            wait (bool, optional): Optionally wait for a response, send e-stop signal if it times out. Defaults to False.
            check (bool, optional): Optionally check each response for an "ok", send e-stop signal if it times out. Defaults to False.
            timeout (float, optional): Timeout for 'wait' and 'check'. Defaults to 10.0.
            
        Returns:
            list: The list of rpc_commands.
        """

        logging.debug("Running gcode protocol with commands: " + pformat(gcode_commands))
            
        # Generate RPC commands from GCODE commands
        # NOTE: Only the dictionary is kept, and it's ID is discarded (hence the "[0]").
        rpc_commands = [self.make_gcode_cmd(cmd)[0] for cmd in gcode_commands]
        
        # Send protocol
        await self.run_rpc_protocol(rpc_commands=rpc_commands,
                                    wait=wait,
                                    check=check,
                                    timeout=timeout)
        
        return rpc_commands

    async def run_rpc_protocol(self, rpc_commands: list, wait=True, check=True, timeout=10.0):
        """Send a list of RPC commands to Moonraker.
        
        Optionally wait for (and/or check) the return status of each command. True by default, as a protocol should stop on failures.

        Args:
            rpc_commands (list): List of RPC/JSON commands for Moonraker.
            wait (bool, optional): Optionally wait for a response, send e-stop signal if it times out. Defaults to False.
            check (bool, optional): Optionally check each response for an "ok", send e-stop signal if it times out. Defaults to False.
            timeout (float, optional): Timeout for 'wait' and 'check'. Defaults to 10.0.

        Returns:
            list: The list of rpc_commands.
        """

        logging.debug("run_rpc_protocol: Running RPC protocol with commands: " + pformat(rpc_commands))
    
        # Wait for printer ready.
        await self.wait_for_ready()

        # Wait for writer coroutine idle.
        await self.wait_for_idle()
        # Flag busy.
        self.idle = False

        # Enable printer ready flag.
        self.ensure_printer_ready = True

        # Reset writer RPC command list.
        self.commands = []

        # Log
        logging.info("run_rpc_protocol: ready for streaming!")

        # Send commands directly unless the "background" writer mode is enabled.
        if not self.background:
            for command in rpc_commands:
                logging.debug("run_rpc_protocol: sending command" + pformat(command))
                cmd_id, checks = await self.send_cmd(rpc_cmd=command, wait=wait, check=check, timeout=timeout)
        else:
            # Only use the "background" writer mchanism if it is enabled.
            logging.info("run_rpc_protocol: streaming using the background system.")
            # Extend the commands list, and let the background writer loop handle it.
            self.cid_overwrite = False
            self.commands.extend(rpc_commands)

            # Optionally wait for a response, send e-stop signal if it times out.
            if wait:
                for command in rpc_commands:
                    cmd_id = command["id"]
                    result = await self.check_command_response(cmd_id=cmd_id, timeout=timeout, loop_delay=0.2)
                    if not result:
                        # Send e-stop if no response was received.
                        logging.error("run_gcode_protocol: no response, sending emergency stop.")
                        await self.emergency_stop()
                        await self.stop()
                        # Quit
                        return False
                    if not self.printer_ready:
                        logging.error("run_gcode_protocol: aborted, printer not ready.")
                        # Quit
                        return False

            # Optionally check each response for an "ok", send e-stop signal if it times out.
            if check:
                for command in rpc_commands:
                    cmd_id = command["id"]
                    result = await self.check_command_result_ok(cmd_id=cmd_id, timeout=timeout, loop_delay=0.2)
                    if not result:
                        # Send e-stop if the response was not 'ok'.
                        logging.error("run_gcode_protocol: no response, sending emergency stop.")
                        await self.emergency_stop()
                        await self.stop()
                        # Quit
                        return False
                    if not self.printer_ready:
                        logging.error("run_gcode_protocol: aborted, printer not ready.")
                        # Quit
                        return False

            # Optionally wait for the command list to empty.
            # NOTE: Checking for "self.background" is redundant. This was ensured by the parent ifelse clause.
            if wait and self.background:
                await self.wait_for_idle()
                assert len(self.commands) == 0

        # Flag idle.
        self.idle = True

        # Return the RPC commands.
        return rpc_commands
    
    def save_gcode_protocol(self, gcode_commands, file="/tmp/protocol.gcode"):
        """Save a list of commands to a text (.gcode) file.

        Args:
            gcode_commands (List): A list of strings.
            file (str, optional): Path to the target text file. Defaults to "/tmp/protocol.gcode".

        Returns:
            bool: Exit code.
        """
        try:
            with open(file, "w") as f:
                for gcode_command in gcode_commands:
                    f.write(gcode_command)
                    f.write('\n')
                
            logging.info("save_gcode_protocol: gcode saved, emitting alert.")
            self.send_alert(message=f"GCODE protocol saved to file {file}.")
                    
        except Exception as e:
            msg = f"save_gcode_protocol: an exception occured: {e}"
            logging.error(msg)
            print(msg)
            traceback.print_exc()
            return False
        
        return True
    
    def send_alert(self, message):
        if self.sio.connected:
            # https://stackoverflow.com/a/61331974
            try:
                loop = asyncio.get_running_loop()
            except RuntimeError:  # 'RuntimeError: There is no current event loop...'
                loop = None
                
            if loop and loop.is_running():
                logging.info('send_alert: Async event loop already running. Adding coroutine to the event loop.')
                tsk = loop.create_task(self.sio_alert(text=message))
            else:
                logging.info('send_alert: Starting new event loop')
                result = asyncio.run(self.sio_alert(text=message))
        else:
            logging.warn(f"send_alert: SIO not connected, message not sent: {message}")
    
    async def get_supported_ungcodes(self):
        """GCODE help for Klipper.
        
        Some of the Klipper commands are GCODE, others are not. Hence "ungcodes".
        
        See: https://moonraker.readthedocs.io/en/latest/web_api/#get-gcode-help
        """
        gcode_help_cmd = rpc_primitives.gcode_help(id=4645)

        cmd_id, checks = await self.send_cmd(rpc_cmd=gcode_help_cmd, wait=True, check=True, timeout=3.0)

        gcode_help_result = self.get_result_by_id(cmd_id=cmd_id)

        return gcode_help_result

    async def test(self):
        print("KlipperCommander test")
        await super().test()
        await asyncio.sleep(0.2)
