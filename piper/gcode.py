# Third-party imports
import re, time, json, pprint, logging, importlib, sys, traceback
from math import pi, copysign
from copy import copy, deepcopy

# Internal imports
from . import gcode_primitives
from . import gcode_utils


class GcodeBuilder:
    """
    This class generates GCODE from Pipettin protocol "actions".

    It holds and tracks machine status or configuration during prococol 'slicing'.
    
    Also stores all GCODE commands after parsing protocols.
    
    Some actions might depend on previous actions or action history.
    So these objects are for tracking the state of the machine during the translation of actions to GCODE,
    or during gcode streaming to the machine controllers.

    Example:

        # Load modules.
        from pprint import pprint
        import os, sys

        # Change working directory.
        os.chdir("/tmp")
        print(os.path.abspath(os.curdir))

        # Append custom module paths.
        module_path = os.path.expanduser("~/Projects/GOSH/gosh-col-dev/pipettin-grbl/klipper/code/")
        sys.path.append(module_path)
        # Load piper modules.
        from piper.commander_utils_mongo import MongoObjects
        from piper.gcode import GcodeBuilder

        # Setup database tools.
        mo = MongoObjects(mongo_url='mongodb://localhost:27017/', verbose=False)

        # List protocols (will print possible protocol names).
        protocols = mo.listProtocols()

        # Get the last protocol.
        protocol = protocols[-1]
        protocol_name = protocol["name"]

        # Manually choose a protocol.
        # protocol_name = "Mixing test 2023-02-05T22:59:49.376Z"

        # Get everything about this protocol by its name.
        print(protocol_name)
        protocol, workspace, platforms_in_workspace = mo.getProtocolObjects(protocol_name=protocol_name)

        # Inspect the protocol.
        pprint(protocol)

        # Instatiate the builder class, specifying tool parameters (pipettes).
        builder = GcodeBuilder(protocol, workspace, platforms_in_workspace, verbose = True)

        # Generate GCODE for the task, it is saved in the task class object.
        builder.parseProtocol()

        # Get the command list, and print it.
        pprint(builder.commands)
    """

    def __init__(self, protocol=None, workspace=None, platformsInWorkspace=None,
                 pipettes=None,
                 plugins=(), config={},
                 verbose=False, velocity_mm_s=1000, tool_velocity_mm_s=200):
        """
        A class to contain all information for the pipetting task: protocol, workspace, platforms and tools.
        :param protocol: A protocol dictionary (JSON object) from the GUI/MongoDB, containing protocol 'actions'.
        :param workspace: A workspace dictionary (JSON object) from the GUI/MongoDB, containing the platform instances in the workspace.
        :param platformsInWorkspace: A platforms dictionary (JSON object) from the GUI/MongoDB, containing the definitions of platforms in the workspace.
        :param pipettes: A dictionary with pipette tool definitions.
        :param config: TODO: use this option to load default options from a JSON configuration file.
        """

        # Save configuration.
        self.config = config

        self.verbose = verbose
        if self.verbose:
            logging.info("Hi! initializing the pipettin' task construct.")

        # GCODE commands list.
        self.commands = []
        # Current Action property.
        self.current_action = None

        # Set workspace, platforms, and workspace properties.
        self.initialize_objects(protocol, workspace, platformsInWorkspace)
        
        # List of available macros for the requested actions
        # TODO: implement repeat dispensing
        # TODO: implement reverse pipetting
        # TODO: implement available/maximum volume checks
        self.action_switch_case = {
            "HOME": self.actionHOME,
            "PICK_TIP": self.macroPickNextTip,
            "LOAD_LIQUID": self.macroGoToAndPipette,
            "DROP_LIQUID": self.macroGoToAndPour,
            "DISCARD_TIP": self.actionDISCARD_TIP,
            "PIPETTE": self.macroPipette,
            "COMMENT": self.actionComment,
            "HUMAN": self.actionHuman,
            "WAIT": self.actionWait,
            "MIX": self.macroGoToAndMix
        }

        # TODO: base these values on a config file
        self.tip_probing_distance = 5.0       # Distance above item's height, from which probing will begin.
        self.probe_extra_dist = 1.0           # Just extra (downwards) probing distance. Harmless if the probe works.
        self.extra_clearance = 1.0 + self.tip_probing_distance

        # Maximum travel distances for each axis
        # TODO: base these values on Klipper's config.
        self.limits = gcode_utils.LIMITS
        
        # Set feedrate, in mm/min (for the "F" GCODE parameter).
        self.feedrate = velocity_mm_s * 60              # Convert from mm/s to mm/min
        # Use a different feedrate for the extrusion/pipetting moves.
        self.tool_feedrate = tool_velocity_mm_s * 60    # Convert from mm/s to mm/min

        # State tracking variables
        self.homed = False
        self.tip_loaded = False
        self.default_state = {
            "s": 0, "vol": 0,  # pipette axis position in mm (s) and microliters (vol).
            "tip_vol": 0,
            "lastdir": None,
            "x": None,
            "y": None,
            "z": None,
            "platform": None,
            "tube": None,
            "paused": False,
            "alarm": False
        }

        # Initialize empty state
        self.state = deepcopy(self.default_state)

        # Available micro-pipette models and characteristics.
        self.pipette = None
        # TODO: base these values on a config file.
        if pipettes is None:
            # Get the pipette definitions.
            self.pipettes = gcode_utils.PIPETTES
        else:
            self.pipettes = pipettes

        # Print pipettes
        if self.verbose:
            logging.debug("GcodeBuilder: Available pipette models on startup: " + pprint.pformat(self.pipettes))

        # Initialize empty state on each pipette
        for p in list(self.pipettes):
            self.pipettes[p]["state"] = deepcopy(self.state)

        # Setup tool tracking
        self.new_tool = None
        self.current_tool = None
        self.toolchange = False
        
        # To track the direction of the last pipette move (for compensation)
        self.lastdir = None

        # Loop over the requrested plugins, which must be the names of
        # python files (without ".py" extension), and "load" them into a dictonay property.
        self.plugins={}
        # Load python modules from the 'plugins' directory through their 'load_gcoder' function.
        for module_name in plugins:
            # Load the requested module from the "plugins" sundirectory.
            module = importlib.import_module("piper.plugins." + module_name)
            # Save the module to the plugins list.
            self.plugins[module_name] = module.load_gcoder(self)

        # __init__ END.


    def initialize_objects(self, protocol, workspace, platformsInWorkspace):
        """Set or reset protocol, workspace, and platforms properties.
        This can be useful when switching protocols or streaming actions, and still preserve state tracking.
        """
        # Define protocol property.
        self.protocol = {}
        if protocol is not None:
            self.protocol = protocol
        # Define workspace property.
        self.workspace = workspace
        # Define platforms property.
        self.platformsInWorkspace = platformsInWorkspace

        # Clearance and probing parameters.
        # NOTE: Using @propery decorator below to defer getClearance() calculation until a workspace is available.
        #       This must be run because otherwise the getter method can fail with "AttributeError".
        if self.platformsInWorkspace:
            self.clearance = self.getClearance()  # requires self.platformsInWorkspace
        else:
            self.clearance = None    

    #### Properties ####
    # See:  https://www.programiz.com/python-programming/property
    #       https://stackoverflow.com/a/51342825/11524079
    # Getter method for "self.clearance". Note that the "clearance" attribute is now a "property",
    # and that the actual value of interest is stored in the new "_clearance" private attribute.
    @property
    def clearance(self):
        # try:
        #     val = self._clearance
        # except AttributeError:
        #     logging.debug("Value not found, trying to get clearance from the platforms in workspace.")
        #     val = self._clearance = self.getClearance()
        if self._clearance:
            logging.debug("Getting clearance value...")
            val = self._clearance
        else:
            logging.debug("Calculating clearance value...")
            val = self.getClearance()
        return val
    # Setter method for "self.clearance".
    @clearance.setter
    def clearance(self, value):
        if self.verbose:
            logging.debug(f"Setting clearance value='{value}'...")
        self._clearance = value

    #### Convenienve math functions ####
    @staticmethod
    def sign(x):
        return copysign(1, x)

    #### Convenienve data handling methods ####
    def getWorkspaceItemByName(self, platform_name):
        """Iterate over items in the workspace looking for one who's name matches 'platform_name' """
        for item in self.workspace["items"]:
            if item["name"] == platform_name:
                return item
            else:
                continue
        return None

    def getPlatformByName(self, platform_item):
        """Iterate over platforms in workspace looking for one who's name matches the platform in 'platform_item' """
        for platform in self.platformsInWorkspace:
            if platform["name"] == platform_item["platform"]:
                return platform
            else:
                continue
        return None

    @staticmethod
    def filterTubesBy(content, _selector):
        """Helper function to use the tube "selector" dict in python's filter()"""
        if content["type"] == "tube" and content[_selector["by"]] == _selector["value"]:
            return True
        else:
            return False

    def getTubeByAction(self, _action):
        """
        Get a tube by an action's selector argument.
        """
        platform_item = self.getWorkspaceItemByName(_action["args"]["item"])
        
        tube = self.getTube(
            selector=_action["args"]["selector"], 
            platform_item=platform_item
        )
        
        return tube
    
    def getTube(self, selector, platform_item):
        """
        Get the next content item from a platform's content.
        """
        tube = next(filter(lambda content: self.filterTubesBy(content, selector),
                           platform_item["content"]))
        return tube
    
    def getContentXY(self, content, platform_item, platform):
        """
        Calculate XY of content center. 
        Defaults to using the col/row pair or, if unavailable, 
        pre-existing x/y coordinates in the content's position.
        """
        
        # Calculate X of content center
        if "col" in content["position"]:
            _x = platform_item["position"]["x"]
            _x += platform['firstWellCenterX']
            _x += (content["position"]["col"] - 1) * platform['wellSeparationX']
        else:
            _x = content["position"]["x"]
        
        # Calculate Y of content center
        if "row" in content["position"]:
            _y = platform_item["position"]["y"]
            _y += platform['firstWellCenterY']
            _y += (content["position"]["row"] - 1) * platform['wellSeparationY']
        else:
            _x = content["position"]["y"]
        
        return _x, _y
    
    def getContentZ(self, content=None, platform_item=None, platform=None):
        """
        Calculate the "active" Z height of a content (e.g. a "tube"). 
        Defaults to using the Z from the content or, if unavailable, information from the platform.

        The platform must contain a "defaultBottomPosition" key with a numeric value.
        Tool-offsets from the "tools dictionary" (see "PIPETTES" at gcode_utils.py) are
        applied to this heigh later on.

        TODO: We could also use the "defaultLoadBottom" coordinate from the platform, which
        should contain a field similar to the one in tips: "defaultLoadBottom": {"p200": 27, "p20": 20}
        Where each key has a corresponding key in the "tools" list (see "PIPETTES" at gcode_utils.py).
        """
        
        # TODO: check and tune Z positioning according to tip 
        #       seal pressure or distance, or the top of the solution.
        # TODO: This might need calibration. Ver issue #7 y #25
        # https://github.com/naikymen/pipettin-grbl/issues/7
        # https://github.com/naikymen/pipettin-grbl/issues/25
        
        # Default to using the Z defined in the content.
        if content is not None:
            if "position" in content:
                if "z" in content["position"]:
                    return content["position"]["z"]
        
        # Use the platform item to get the platform if undefined.
        if (platform_item is not None) and (platform is None):
            platform = self.getPlatformByName(platform_item)
        
        # Get the default Z from the platform.
        if platform is not None:
            # Z-coordinate of the bottom of the tube (i.e. relative to the reference tool, with offsets [0,0,0]).
            # Tool-specific offsets defined in the tools list will be used to adjust this later on.
            z = platform["defaultBottomPosition"]
            # Alternative using tool positions hardcoded in platforms:
            # z = platform["defaultLoadBottom"][self.current_tool]
            return z

        # Raise an error if the inut did not match any condition above.
        raise Exception("getContentZ: no condition matched.")
    
    def addToolOffsets(self, x=None, y=None, z=None, tool:str=None):
        """Add offsets of the current tool.

        Args:
            x (float, optional): Value of the x coordinate to offset. Defaults to None.
            y (float, optional): Value of the y coordinate to offset. Defaults to None.
            z (float, optional): Value of the z coordinate to offset. Defaults to None.
            tool (str, optional): Name of a tool. Defaults to None.

        Returns:
            tuple: An updated coordiante set, with one element per each non-None coordinate argument.
        """
        if tool is None:
            # Use the current tool if none was provided.
            if self.current_tool:
                logging.debug(f"gcodeBuilder message:    " + f"adding offsets with self.current_tool={self.current_tool}")
                tool = self.current_tool
            # If there is not current tool, then return the input.
            else:
                logging.debug("gcodeBuilder message:    " + "no offsets added, self.current_tool was 'None'.")
                return tuple([i for i in (x, y, z) if i is not None])
        
        coords = tuple()
        
        if x is not None:
            coords += (x + self.pipettes[tool]["tool_offset"]["x"], )
            
        if y is not None:
            coords += (y + self.pipettes[tool]["tool_offset"]["y"], )
            
        if z is not None:
            coords += (z + self.pipettes[tool]["tool_offset"]["z"], )
        
        return coords

    def getTubeCoords(self, _action):
        """
        Get XYZ coordinates for the action's platform item (a "tube" in this case).
        
        TODO: rename to "getContentCoords".
        
        Also apply tool offsets, if any.
        """

        # Get platform and platform item
        platform_item = self.getWorkspaceItemByName(_action["args"]["item"])
        platform = self.getPlatformByName(platform_item)
        
        # Get tube
        selector = _action["args"]["selector"]
        tube = self.getTube(selector, platform_item)

        # Calculate XY of tube center           
        _x, _y = self.getContentXY(tube, platform_item, platform)

        # Calculate Z for pipetting
        _z = self.getContentZ(platform=platform)
        
        # Add the current tool's offsets
        if self.current_tool is not None:
            _x, _y, _z = self.addToolOffsets(x=_x, y=_y, z=_z, tool=self.current_tool)

        return _x, _y, _z

    #### Convenience functions for clearance coordinates ####
    def getClearance(self):
        """Get the minimum clearance level for the Z axis from the platformsInWorkspace definition"""
        return max([float(platform["defaultTopPosition"]) for platform in self.platformsInWorkspace])

    def getSafeHeight(self):
        """Calculate the Z coordinate above all contents, considering if a tip is placed on the current tool."""

        # Calculate Z coordinate
        _z = self.clearance
        _z += self.extra_clearance

        # # Add tool offset if any
        # if self.current_tool is not None:
        #     _z += self.pipettes[self.current_tool]["tool_offset"]["z"]
        # elif self.verbose:
        #     logging.debug("gcodeBuilder message:    " + "no Y offset added, self.current_tool was None.")

        # Add tip offset if any
        if self.tip_loaded:
            _z += self.tip_loaded["tipLength"]
            # Add tool offset if any
            if self.current_tool is not None:
                _z += self.pipettes[self.current_tool]["tool_offset"]["z_tip"]
        # If no tips are loaded
        else:
            # But a tool is loaded
            if self.current_tool is not None:
                # Then add the "bare" tool offset
                _z += self.pipettes[self.current_tool]["tool_offset"]["z_bare"]

        return _z

    def getSafeY(self):
        """
        There is a limit on the Y axis movement. The tool-holder can crash into the parked tools.
        Here we use the parking post coordinates to establish a safe limit for the Y direction.
        TODO: we will eventually need safe "area" checks, for coordinates _and_ for paths.
        @return:
        """

        # Calculate Y coordinate
        safe_y = self.limits["y_max"]
        for p in list(self.pipettes):
            safe_y = min(safe_y, self.pipettes[p]["tool_post"]["y"])

        # Add tool length offset if the carriage is loaded with a tool.
        if self.current_tool is not None:
            y_clear = self.pipettes[self.current_tool]["tool_post"]["y_clearance"]
            safe_y -= y_clear
            if self.verbose:
                logging.debug("gcodeBuilder message:    " + f"{self.current_tool} y_clearance offset added: {y_clear}")
        elif self.verbose:
            logging.debug("gcodeBuilder message:    " + "no Y offset added, self.current_tool was None.")

        return safe_y

    #### Liquid handling correction functions ####
    def capillary_corr(self, final_vol):
        """
        Define reciprocal function.
        Linear function. For the p200 pipette, returns -2 at final volume 0, and 0 at final volume 20 (and above).
        """
        
        start_vol = self.pipettes[self.current_tool]["tension_correction"]["start_vol"]
        max_correction = self.pipettes[self.current_tool]["tension_correction"]["max_correction"]
        
        # Must be negative to dispense volume
        capillary_extra_vol = -abs(-final_vol * max_correction / start_vol + max_correction)
        
        # Cap maximum correction volume
        correction = max([-max_correction, capillary_extra_vol])  
        
        return correction
        # return -abs((-1/(21-x) + 0.04761905) * (2/0.952381))  # reciprocal attempt, KISS!

    #### Action "macros" to convert actions into GCODE ####
    def actionComment(self, _action, _i, *args):
        comment_command = [gcode_primitives.comment(message=_action["args"]["text"])]
        
        self.commands.extend(comment_command)
        self.current_action["GCODE"].extend(comment_command)
        
        return "Processed actionComment with index " + str(_i) + " and text: '" + _action["args"]["text"] + "'"

    def actionWait(self, _action, _i, *args):
        wait_command = gcode_primitives.gcodeDwell(seconds=_action["args"]["seconds"])
        # wait_command = [self.wait_prefix + str(_action["args"]["seconds"]*1000)]
        
        self.commands.extend(wait_command)
        self.current_action["GCODE"].extend(wait_command)
        
        return f'actionWait executed for action {_i} with time {_action["args"]["seconds"]}'

    def actionHuman(self, _action, _i, *args):
        # TODO: human intervention action ID.
        # TODO: human intervention timeout behaviour.
        
        human_command = gcode_primitives.gcodeHuman(_action["args"]["text"])
        # human_command = [self.human_prefix + _action["args"]["text"]]
        
        self.commands.extend(human_command)
        self.current_action["GCODE"].extend(human_command)
        
        return "Pause in action " + str(_i) + " with text " + _action["args"]["text"]

    def actionHOME(self, _action, _i, *args, do_final_g92=False):

        # XYZ: parse which axes to home
        if "args" in _action.keys():
            raise Exception("actionHOME: args not supported yet.")

        # XYZ
        home_sweet_home = gcode_primitives.gcodeHomeXYZ()
        self.commands.extend(home_sweet_home)
        self.current_action["GCODE"].extend(home_sweet_home)

        # Tools: home all if any requested
        # TODO: implement per-tool homing. Protocols can only home at start/end, not critical for now.
        self.macroPipetteHome()

        return "Processed actionHOME with index " + str(_i)

    def actionDISCARD_TIP(self, _action, _i, *ignored_args, **ignored_kwargs):
        """Move over the trash box and discard the tip (if placed, else Except).

        Action moves:
            - Move to clearance level on Z (avoiding platforms) and Y (avoiding parked tools).
            - Move to the ejection post and eject the tip.
            - Home the tool.

        Args:
            _action (dict): The action definition. Contents not used. The coordinates are loaded from the tool/pipette definition.
            _i (int): The index of the action. Not used.

        Raises:
            Exception: An error is raised when there are no tips to discard.

        Returns:
            str: Generic message.
        
        Sample action:
            {
                "cmd": "DISCARD_TIP",
                "args": {
                    "item": "descarte 1"
                }
            }
        """
        # _action = self.protocol["actions"][4]

        # TODO: should a warning be issued about the ignored "item" key in the action's args?

        if self.tip_loaded:
            self.macroMoveSafeHeight()  # move to clearance level on Z, should extend task.commmand list automatically

            # Generate tip ejection post macro
            safe_z = self.getSafeHeight()
            safe_y = self.getSafeY()
            eject_coords = self.pipette["eject_post"]
            eject_commands = gcode_primitives.gcodeEjectTip(**eject_coords,
                                                           safe_z=safe_z, safe_y=safe_y,
                                                           feedrate=self.feedrate)
            # Add them to the list
            self.commands.extend(eject_commands)
            self.current_action["GCODE"].extend(eject_commands)

            # TODO: wtf will be the drop tip GCODE
            self.tip_loaded = False
            self.macroMoveSafeHeight()  # Raise to safety. Should do an automatic append

            # Ensure zeroed pipette axis
            # self.macroPipetteZero()
            # NOTE: replace macroPipetteZero with extruder homing command.
            self.macroPipetteHome(which_tool=self.current_tool)
            # TODO: reimplement non-homing zeroing once the underlying Klipper issue is resolved:
            #   Sending "M82" and "G0 E0.0" does not set the position to 0, instead GET_POSITION responds:
            #       toolhead: X:0.000000 Y:0.000000 Z:150.000000 E:14.998750
            #       gcode: X:0.000000 Y:0.000000 Z:150.000000 E:14.998750
            #       gcode base: X:0.000000 Y:0.000000 Z:0.000000 E:14.998750

            # Register no tip in the volume tracker
            # self.state["tip_vol"] = None
            self.pipettes[self.current_tool]["state"]["tip_vol"] = None

        else:
            # TODO: discutir si es mejor tirar este error o no hacer nada
            raise Exception(f"PROTOCOL ERROR: Cannot discard tip if one is not already placed! Action index: {_i}")

        return f"Processed actionDISCARD_TIP with index {_i}"

    def macroMoveSafeHeight(self, _mode="G0", absolute=True):
        """G0 to safe height in Z axis, considering loaded tipLength"""
        clear_comment = [gcode_primitives.comment("Moving to the clearance position.")]
        move_commands = gcode_primitives.gcodeMove(z=self.getSafeHeight(), f=self.feedrate, 
                                                  _mode=_mode, absolute=absolute)
        commands = clear_comment + move_commands
        self.commands.extend(commands)
        self.current_action["GCODE"].extend(commands)
        return commands

    def macroPipette(self, _action, _i, 
                     backlash_correction=True, 
                     tip_priming_correction=True,
                     back_pour_correction=True,
                     capillary_correction=True,
                     check_volume_limits=True):
        """
        Pipetting works incrementally for now (i.e. relative volumes, not absolute volumes).
        Negative values mean 'dispense' or 'lower' the axis (see functions in driverSaxis.py).
        """
        volume = _action["args"]["volume"]
        
        if self.verbose:
            # Comment GCODE
            message = [f" ; Pipetting {volume} uL with {self.current_tool}"]
            self.commands.extend(message)
            self.current_action["GCODE"].extend(message)
            logging.debug(f"macroPipette: pipetting {volume} uL for action {_i}.")

        # Volume limit check
        max_tip_volume = self.tip_loaded["maxVolume"]
        final_volume = self.tip_loaded.get("volume", 0.0) + volume
        if check_volume_limits and final_volume > max_tip_volume:
            raise Exception(f"macroPipette ERROR on action {_i}: the requested final volume ({final_volume}) exceeds the tip's capacity of {max_tip_volume}")

        # Backlash compensation.
        if backlash_correction:
            # If previous and current directions are different
            last_dir_sign = self.sign(self.pipettes[self.current_tool]["state"]["lastdir"])
            new_dir_sign = self.sign(volume)
            if (last_dir_sign * new_dir_sign) == -1:
                # Then over-pour (negative volume, -0.5 uL for p200) or over-draw (positive volume, 0.5 uL for p200)
                backlash_correction = self.sign(volume) * self.pipette["backlash_compensation_volume"]
                volume += backlash_correction
                if self.verbose:
                    # Comment GCODE
                    message = [f" ; Action {_i}: pipetting extra {backlash_correction} uL on backlash correction."]
                    self.commands.extend(message)
                    self.current_action["GCODE"].extend(message)
            # TODO: be smarter about backlash

        # Compensation for under-drawing on first load.
        # Check if the tip is new/empty.
        first_draw = self.pipettes[self.current_tool]["state"]["tip_vol"] is None
        # The correction is optional.
        if first_draw and tip_priming_correction:
            # If so, we are drawing volume for the first time with the tip.
            # In which case we must over-draw by a specified amount (e.g. 5 uL for the p200).
            # Apply correction.
            # TODO: testing normal pre-wetting the tip instead.
            # volume += self.pipette["extra_draw_volume"]
            if self.verbose:
                # Comment GCODE
                message = [f" ; Action {_i}: pipetting extra {self.pipette['extra_draw_volume']} uL on first draw."]
                self.commands.extend(message)
                self.current_action["GCODE"].extend(message)
            
            # Limit pre-wetting to 120% of move volume or max volume.
            correction_volume = min(abs(self.pipette["vol_max"]), volume*1.2)
            
            # Apply the correction by calling this very method, by pipetting up and down
            # (i.e. pre-wetting the tip).
            for s in [1.0, -1.0]:
                new_action = {"args": {"volume": correction_volume*s}}
                # Move without corrections
                self.macroPipette(new_action, _i, 
                                  backlash_correction=False, 
                                  tip_priming_correction=False,
                                  back_pour_correction=False,
                                  capillary_correction=False)

        # Compensation for capillary effects for low final tip volume during dispensing.
        # Apply only for regular dispensing moves.
        if capillary_correction and self.sign(volume) == -1 and not first_draw:
            # Apply correction if necessary: only at low final volumes.
            correction_start_vol = self.pipettes[self.current_tool]["tension_correction"]["start_vol"]
            
            # Get the current tip volume (i.e. from the previous action).
            final_volume = self.pipettes[self.current_tool]["state"]["vol"]
            
            # Add the new [corrected] volume to the local pipette volume tracker (axis position in uL).
            final_volume += volume

            # Subtract the "back-pour correction" volume if this is a first draw,
            # forcing negative volume (for dispensing). Though this "correction" is 
            # applied later on, it must be considered here, because it will change 
            # the final volume. 
            # TODO: disabled this bit for now, since this correction is meant for 
            #       simple dispense moves only.
            # if first_draw and tip_priming_correction and back_pour_correction:
            #     final_volume += -abs(self.pipette["back_pour_correction"])
            
            # Apply the correction.
            if final_volume < correction_start_vol:
                # Compute the "extra" volume that must be dispensed.
                capillary_extra_vol = self.capillary_corr(final_volume)
                
                # Comment GCODE
                if self.verbose:
                    message = [f" ; Action {_i}: final volume {final_volume} uL, pipetting extra {capillary_extra_vol} uL."]
                    self.commands.extend(message)
                    self.current_action["GCODE"].extend(message)
                
                # Apply correction
                volume += capillary_extra_vol

        # Compute axis displacement from the target pipetting volume.
        # Default pipetting mode should be incremental (i.e. relative) volume.
        if "mode" not in _action["args"].keys():
            _action["args"]["mode"] = "incremental"
        # Convert volume to shaft displacement in millimeters in incremental mode.
        if _action["args"]["mode"] == "incremental":
            # Units are in microliters, they must be converted.
            s_axis_movement = volume * self.pipette["scaler_adjust"] / self.pipette["scaler"] 
        # TODO: implement "absolute" pipetting volumes.
        else:
            raise Exception(f"PROTOCOL ERROR: invalid macroPipette mode at action with index {_i}")

        # Comment GCODE
        if self.verbose:
            logging.debug("gcodeBuilder message:    " +
                  f"Volume {volume} converted to {s_axis_movement} mm displacement" +
                  f" with scaler: {self.pipette['scaler']}")
            message = [f" ; Action {_i}: Converted {volume} uL to {s_axis_movement} mm with scaler: {self.pipette['scaler']}"]
            self.commands.extend(message)
            self.current_action["GCODE"].extend(message)

        # Compose GCODE command.
        # TODO: add pipetting speed control.
        # TODO: volume sign INVERTED here, be smarter about the direction.
        command = gcode_primitives.gcodeMove(e=-s_axis_movement, f=self.tool_feedrate)

        # Add the pipetting command
        self.commands.extend(command)
        self.current_action["GCODE"].extend(command)

        # Setup valid tip volume before updating
        if self.pipettes[self.current_tool]["state"]["tip_vol"] is None:
            self.pipettes[self.current_tool]["state"]["tip_vol"] = 0
        
        # Update pipette state
        self.pipettes[self.current_tool]["state"]["s"] += s_axis_movement
        self.pipettes[self.current_tool]["state"]["vol"] += volume
        self.pipettes[self.current_tool]["state"]["tip_vol"] += volume
        self.tip_loaded["volume"] = volume + self.tip_loaded.get("volume", 0.0)
        
        # Update direction with the current move
        self.pipettes[self.current_tool]["state"]["lastdir"] = self.sign(volume)
        # Print state
        if self.verbose:
            logging.debug(f"gcodeBuilder message:    Printing state of tool {self.current_tool}: " + pprint.pformat(self.pipettes[self.current_tool]["state"]))

        # Back-pour correction
        # If it is a first draw, try correcting "liquid backlash" by over-drawing and pouring a bit
        # back into the tube. This corrects two issues:
        #   1. Under drawing in the first load.
        #   2. Over pouring in the first dispense.
        if first_draw and back_pour_correction:
            # Get parameters from the tool
            under_draw_correction = self.pipette["under_draw_correction"]
            back_pour_correction = self.pipette["back_pour_correction"]
            
            # Over-draw
            vol_correction = abs(back_pour_correction) + abs(under_draw_correction) # negative, force loading
            new_action = {"args": {"volume": vol_correction}}  # in microliters
            if self.verbose:
                logging.debug("gcodeBuilder message:    " + f"Using back-pour correction with volume {vol_correction}")
                message = [f" ; Action {_i}: applying {vol_correction} uL over-draw volume correction."]
                self.commands.extend(message)
                self.current_action["GCODE"].extend(message)
            # Apply the correction by calling this very method.
            self.macroPipette(new_action, _i, 
                              backlash_correction=False, 
                              tip_priming_correction=False,
                              back_pour_correction=False,
                              capillary_correction=False)
            
            # Back-pour
            vol_correction = -abs(back_pour_correction)   # negative, force dispensing
            new_action = {"args": {"volume": vol_correction}}  # in microliters
            if self.verbose:
                logging.debug("gcodeBuilder message:    " + f"Using back-pour correction with volume {vol_correction}")
                message = [f" ; Action {_i}: applying {vol_correction} uL back-pour volume correction."]
                self.commands.extend(message)
                self.current_action["GCODE"].extend(message)
            # Apply the correction by calling this very method.
            self.macroPipette(new_action, _i, 
                              backlash_correction=False, 
                              tip_priming_correction=False,
                              back_pour_correction=False,
                              capillary_correction=False)

        return "Processed macroPipette action with index " + str(_i)

    def macroPipetteZero(self):
        """
        Move the pipette to the zero position (without re-homing).
        
        Note: this now requires that the pipette homes upwards to the axis origin (i.e. at 0)
        and the retraction distance is considered the zero position for pietting.
        """
        # Reverse net displacement
        # volume = -self.state["vol"]
        volume = -self.pipettes[self.current_tool]["state"]["vol"]
        s_axis_movement = volume * self.pipette["scaler_adjust"] / self.pipette["scaler"] 
        
        if self.verbose:
            logging.debug("gcodeBuilder message:    " +
                  f"macroPipetteZero called with volume={volume} and s={s_axis_movement}")

        # Compose and append the pseudo-GCODE command (with volume units, in microliters)
        # command = self.pipetting_command_prefix + str(s_axis_movement)
        # TODO: add pipetting speed control
        # TODO: use a gcodePrimitives function instead
        # command = [f"{self.pipetting_command_prefix}{s_axis_movement}; {self.current_tool} axis zeroed"]
        
        # NOTE: replace with absolute extruder position command.
        command = [gcode_primitives.comment("Zeroing axis.")]
        command.extend(gcode_primitives.gcodeCoordModeExtruder(absolute=True))
        retraction_pos = self.pipettes[self.current_tool]["retraction_displacement"]
        command.extend(gcode_primitives.gcodeMove(e=retraction_pos))
        command.extend(gcode_primitives.gcodeCoordModeExtruder(absolute=False))
        command.extend([gcode_primitives.comment("Axis zeroed.")])
        
        # Append commands
        self.commands.extend(command)
        self.current_action["GCODE"].extend(command)

        # Update state trackers
        self.pipettes[self.current_tool]["state"]["s"] += s_axis_movement
        self.pipettes[self.current_tool]["state"]["vol"] += volume
        if self.tip_loaded:
            self.pipettes[self.current_tool]["state"]["tip_vol"] += volume
            # Add the volume to the tip (note the default initial value of 0.0).
            self.tip_loaded["volume"] = volume + self.tip_loaded.get("volume", 0.0)
        # Update direction with the current move
        self.pipettes[self.current_tool]["state"]["lastdir"] = self.sign(volume)

        # Check for final volume near zero
        state_vol = abs(self.pipettes[self.current_tool]["state"]["vol"])
        state_s = abs(self.pipettes[self.current_tool]["state"]["s"])
        threshold = 1e-10
        if (state_vol > threshold) or (state_s > threshold):
            raise Exception("PROTOCOL ERROR: pipette was zeroed, but reference states did not match zero. " +
                            "Expected vol=0 and s=0 but got:" +
                            f" vol={self.pipettes[self.current_tool]['state']['vol']}" +
                            f" s={self.pipettes[self.current_tool]['state']['s']}")
        # Done

    def macroPipetteHome(self, which_tool="all", retraction_mm=None, *args):
        """
        This command will be interpreted by the SAxis driver. TO-DO: this code really should be moved to driverSaxis.py
        It will use the limit switches to find the highest position, retract, and then lower it again by 'retraction_mm'
        :param retraction_mm: extra distance to the SAxis limit switch, defaults to "retraction_displacement" in the tool info.
        :param args: extra arguments (ignored for now).

        Args:
            which_tool: "ALL", "P200", "P20" or similar.
        """
        
        # Distance to travel downwards after homing the pipette.
        # After homing the S axis, lower it by retraction_mm millimeters, pressing the pipette shaft up to
        # it's first stop (or just over it, ideally). This position will be set as a zero volume reference point.
        # TODO: Set reference position at home (just after homing).

        if which_tool == "all":
            for pipette_name in list(self.pipettes):
                # Check for retraction_mm override
                if retraction_mm is not None:
                    tool_retraction_mm = retraction_mm
                else:
                    tool_retraction_mm = self.pipettes[pipette_name]["retraction_displacement"]
                # Build the command
                tool_home_cmd = gcode_primitives.gcodeHomeP(which_axis=pipette_name.upper(),
                                                           retraction_mm=tool_retraction_mm)
                # Add the command
                self.commands.extend(tool_home_cmd)
                self.current_action["GCODE"].extend(tool_home_cmd)
        else:
            # Check for retraction_mm override
            if retraction_mm is not None:
                tool_retraction_mm = retraction_mm
            else:
                tool_retraction_mm = self.pipettes[which_tool]["retraction_displacement"]
            # Build the command
            tool_home_cmd = gcode_primitives.gcodeHomeP(which_axis=which_tool, 
                                                       retraction_mm=tool_retraction_mm)
            # Add the tool-specific command
            self.commands.extend(tool_home_cmd)
            self.current_action["GCODE"].extend(tool_home_cmd)

        # Corrección para el "pipeteo de menos" en la primera carga de volumen.
        # Lo que hace es mover la pipeta 20 uL hacia "arriba" (i.e. un movimiento de carga de volumen),
        # despues del homing.
        # s_axis_movement = self.pipette["homing_backlash_compensation"] * self.pipette["scaler_adjust"] / self.pipette["scaler"] 
        # s_axis_movement = abs(s_axis_movement)  # Debe ser positivo para que se eleve.
        # command = [self.pipetting_command_prefix + str(s_axis_movement)]
        # self.commands.extend(command)

        # Zero the volumes
        # self.state["vol"] = 0
        # self.state["s"] = 0
        if which_tool.lower() == "all":
            for p in list(self.pipettes):
                self.pipettes[p]["state"]["vol"] = 0
                self.pipettes[p]["state"]["s"] = 0
                # Update direction with the current move
                self.pipettes[p]["state"]["lastdir"] = self.sign(-1)
        else:
            self.pipettes[which_tool]["state"]["vol"] = 0
            self.pipettes[which_tool]["state"]["s"] = 0
            # Update direction with the current move
            self.pipettes[which_tool]["state"]["lastdir"] = self.sign(-1)

        return "Pipette homed."

    def macroSetupMachine(self):
        """
        Generate XYZ and pipette axis homing commands.
        Optionally probing the Z axis.
        """

        # Build toolhead home commands
        homing_commands = [" ; macroSetupMachine commands START"]
        homing_commands.extend(gcode_primitives.gcodeHomeXYZ())
        
        # TODO: find Klipper equivalent.
        # gcode_primitives.gcodeSetFeedrate(self.feedRateDefault)
        
        # Append XYZ home commands.
        self.commands.extend(homing_commands)
        self.current_action["GCODE"].extend(homing_commands)

        # Home tools
        self.macroPipetteHome()

        # Flag the machine as homed.
        self.homed = True

        return "Processed macroSetupMachine"

    def getNextTip(self, platform_item):
        # Get the next tip and delete it from the original platform_item object
        # So that it won't get picked again
        next_tip = next(content for content in platform_item["content"] if content["type"] == "tip")
        for i, content in enumerate(platform_item["content"]):
            if content["type"] == "tip" and content["index"] == next_tip["index"]:
                del platform_item["content"][i]
                # Referencing will remove it from the task class/workspace object class as well
                # This is the desired (side)effect, but it does not propagate to the mongo database
                break
            else:
                continue
        return next_tip

    def getTipAndCoords(self, action, action_index):
        """Get a tip object and its coordinates.

        Args:
            action (dict): Protocol action definition.
            action_index (int): Action index.

        Returns:
            tuple: Tuple with the tube object, and the xyz coordinates.

        Example "tip":  'index': 96,
                        'maxVolume': 160,
                        'name': 'tip96',
                        'position': {'col': 12, 'row': 8},
                        'tags': [],
                        'type': 'tip',
                        'volume': 0

        The "tip" above will be fetched from the database when the action's "args" are of "item" type:
            'args': {'item': '200ul_tip_rack_MULTITOOL 1', 'tool': 'P200'},
            'cmd': 'PICK_TIP'
        
        The action can also provide the tip's coordinates directly, but must provide "tip" data explicitly:
            'args': {'coords': {"x": 20, "y": 200, "z": 30}, 
                    'tool': 'P200',
                    'tip': tip_definition},  # Note the tip data passed here (described below).
            'cmd': 'PICK_TIP'

        Example "tip_definition" for "coords" type args:
            'maxVolume': 160,
            'tipLength': 50.0,
            'volume': 0

        """
        # Use the provided coordinates if found.
        if "coords" in list(action["args"]):
            # Get coords.
            coords = action["args"]["coords"]
            _x, _y, _z = coords["x"], coords["y"], coords["z"]
            # Get the tip's info. It must contain "tipLength".
            next_tip = action["args"]["tip"]
            # Save information about the loaded tip to the class.
            # NOTE: Ensure that the required data is there.
            #       it must be in (tip) definition, cant be feched from "platform_" in this context.
            next_tip["tipLength"] = action["args"]["tip"]["tipLength"]
            next_tip["maxVolume"] = action["args"]["tip"]["maxVolume"]
            next_tip["volume"] = action["args"]["tip"]["volume"]
            # next_tip["fromAction"] = action_index     # NOTE: commented, its added below.
            # next_tip["inTool"] = self.current_tool    # NOTE: commented, its added below.
        
        # Else lookup the coordinates by the name of the tip box platform.
        else:
            platform_name = action["args"]["item"]
            platform_item = self.getWorkspaceItemByName(platform_name)
            platform_ = self.getPlatformByName(platform_item)

            # Get the next tip from the tip box
            next_tip = self.getNextTip(platform_item=platform_item)
            
            # Calculate XY coordinate
            _x, _y = self.getContentXY(
                content=next_tip,
                platform_item=platform_item, 
                platform=platform_)
            
            # Add XY tool offsets
            _x, _y = self.addToolOffsets(x=_x, y=_y, tool=self.current_tool)
            
            # Old code
            # # Calculate X coordinate
            # _x = platform_item["position"]["x"]
            # _x += platform_['firstWellCenterX']
            # _x += (next_tip["position"]["col"] - 1)* platform_['wellSeparationX']
            # Add X tool offset
            # if self.current_tool is not None:
            #     _x += self.pipettes[self.current_tool]["tool_offset"]["x"]
            # elif self.verbose:
            #     logging.debug("gcodeBuilder message:    " + "no Y offset added, self.current_tool was None.")

            # # Calculate Y coordinate
            # _y = platform_item["position"]["y"]
            # _y += platform_['firstWellCenterY']
            # _y += (next_tip["position"]["row"] - 1)* platform_['wellSeparationY']
            # Add Y tool offset
            # if self.current_tool is not None:
            #     _y += self.pipettes[self.current_tool]["tool_offset"]["y"]
            # elif self.verbose:
            #     logging.debug("gcodeBuilder message:    " + "no Y offset added, self.current_tool was 'None'.")

            # TODO: Check and tune Z positioning according to tip seal pressure,
            #       this might need calibration. Ver issue #7: https://github.com/naikymen/pipettin-grbl/issues/7
            
            # TODO: revert back to tool-specific default load botoom.
            # Get height for tip placement for the current tool (hardcoded in platform definition).
            # _z = platform_["defaultLoadBottom"][self.current_tool]
            # CAUTION: I must add this to the TIP_RACK platform definition: "defaultLoadBottom": {"p200": 27, "p20": 20},

            # TODO: Get the "default" Z position for placement, and adjust using the tool offsets.
            # _z = platform_["defaulBottomPosition"][self.current_tool]
            # _z = self.addToolOffsets(z=_z, tool=self.current_tool)
            
            # TODO: Consider using the "z_bare" offset instead of the "defaultLoadBottom" property.
            # TODO: Check that "z_bare" is the correct offset.
            _z = platform_["defaultLoadBottom"] + self.pipettes[self.current_tool]["tool_offset"]["z_bare"]
            
            # TODO: remember to reactivate adding tip length if necessary.
            # _z += platform_["tipLength"]

            # Get information about the loaded tip from the platform
            next_tip["tipLength"] = platform_["tipLength"]
        
        # Save contextual information to the tip
        next_tip["fromAction"] = action_index
        next_tip["inTool"] = self.current_tool

        # Return the tip and its spatial location
        return next_tip, _x, _y, _z

    def macroPickNextTip(self, _action, _i):
        """
        Will throw an error if a tip is already loaded

        Sample action:
            {
                "cmd": "PICK_TIP",
                "args": {
                    "item": "200ul_tip_rack_MULTITOOL 1",
                    "tool": "P200"
                }
            }

        Likely platformless action:
            {
                "cmd": "PICK_TIP",
                "args": {
                    "coords": {"x": 10.5,"y": 20.5,"z": 0.5},
                    "tool": "P200",
                    "tip": {"tipLength": 50.0, "maxVolume": 200, "volume": 0}
                }
            }
        """

        if not self.tip_loaded:
            # Move to clearance level on Z, extends task.command list automatically
            self.macroMoveSafeHeight()

            # move to next tip location on XY
            # _i = 1
            # _action = protocol["actions"][_i]  # for testing

            # Get tip and coords (supports "coords" key specification).
            next_tip, _x, _y, _z = self.getTipAndCoords(_action, _i)

            # NO-PROBE CODE START ####
            # self.commands.extend([
            #     gcode_primitives.gcodeMove(x=_x, y=_y),  # Move over the tip
            #     gcode_primitives.gcodeMove(z=_z)  # Place tip
            # ])
            # NO-PROBE CODE END ####

            # Probe to place code START ####
            # TODO: make this a vibration if possible (i have not found this necessary).
            probe_to_place = self.pipette["tip_probing_distance"]  # Clearance distance before probing
            probe_extra_dist = self.pipette["probe_extra_dist"]
            # seal_retract = -(_probe_to_place + _sealing_move)
            # probe_final = _seal_retract
            z_scan = -(probe_to_place + probe_extra_dist)

            # Build probe command list
            probe_to_place_command = []
            # Move over the tip on XY
            probe_to_place_command.extend(gcode_primitives.gcodeMove(x=_x, y=_y, f=self.feedrate))
            # Move over the tip on Z
            probe_to_place_command.extend(gcode_primitives.gcodeMove(z=_z+probe_to_place, f=self.feedrate))
            # Probe for the tip, with extra scan distance
            probe_to_place_command.extend(gcode_primitives.gcodePipetteProbe(z_scan=z_scan))

            # Extend commands list
            self.commands.extend(probe_to_place_command)
            self.current_action["GCODE"].extend(probe_to_place_command)
            # Probe to place code END ####

            # Flag loaded tip
            self.tip_loaded = next_tip

            # TODO: if multiple "extruders" are present,
            #  tip loading should do a G92 command with new XY and Z as well.
            # NOTE: I am handling this with tool offsets here, instead of messing with GRBL.

            self.macroMoveSafeHeight()  # Raise to safety. Should do an automatic append

            # Zero the tip volume tracking (set to None instead for new tip check).
            self.pipettes[self.current_tool]["state"]["tip_vol"] = None

            # move to Z just on top
            # TODO: seal the tip by pressing a little bit very slowly two times
            # move to clearance level on Z
        else:
            # TODO: discutir si es mejor descartar el tip
            #  si está automáticamente o tirar este error
            raise Exception("PROTOCOL ERROR: Cannot pick tip if one is already placed! Action index: " + str(_i))

        return "Processed macroPickNextTip action with index " + str(_i)

    # TODO: code the missing action interpreters

    def macroGoToAndPour(self, _action, _i, mode="incremental", *args):
        """
        Make volume "negative" and run macroGoToAndPipette: move to action XZY position and drop volume.

        Sample action:
            {
                "cmd": "DROP_LIQUID",
                "args": {
                    "item": "5x16_1.5_rack 1",
                    "selector": {"by": "name", "value": "tube2"},
                    "volume": 100,
                    "tool": "P200"
                }
            }

        :param _action: and "action" object from the protocol
        :param _i: action index in the protocol
        :param mode: pipetting mode
        :param args: ignore other arguments ignored for now.
        :return: "args" of the action: _action["args"]
        """
        # move to clearance level on Z
        # move to next tube location on XY
        # move to Z at dispensing height
        # dispense liquid
        # flush tip if all must be dropped, consider pipetting mode if reverse or repetitive
        # move to clearance level on Z

        _action["args"]["volume"] = -abs(float(_action["args"]["volume"]))  # Force negative volume

        ret_msg = self.macroGoToAndPipette(_action, _i)

        return ret_msg

    def macroGoToAndPipette(self, _action, _i):
        """
        LOAD_LIQUID action interpreter: moves to XYZ position and moves S axis to pipette.
        Will throw an error if a tip is not already loaded.

        Sample action:
            {
                "cmd": "LOAD_LIQUID",
                "args": {
                    "item": "5x16_1.5_rack 1",
                    "selector": {"by": "name", "value": "tube1"},
                    "volume": 100,
                    "tool": "P200"
                }
            }

        Likely platformless action:
            {
                "cmd": "LOAD_LIQUID",
                "args": {
                    "tube": {"volume": 200.0},
                    "volume": 100,
                    "tool": "P200"
                }
            }
        """
        # move to clearance level on Z
        # move to next tube location on XY
        # move to Z at loading height
        # load liquid
        # move to clearance level on Z

        if self.tip_loaded:
            # move to clearance level on Z
            self.macroMoveSafeHeight()  # should extend task.commands list automatically

            # Get XYZ coordinates for the action's tube,
            # applying current tool offsets if any.
            _tube, _x, _y, _z = self.getTubeAndCoords(_action, _i)

            go_over_tip_commands = []
            # Move over the tip
            go_over_tip_commands.extend(gcode_primitives.gcodeMove(x=_x, y=_y, f=self.feedrate))
            # Go to the bottom of the tube
            go_over_tip_commands.extend(gcode_primitives.gcodeMove(z=_z, f=self.feedrate))
            # TODO: be smarter about pipetting height, using current tube volume to increase this.

            self.commands.extend(go_over_tip_commands)
            self.current_action["GCODE"].extend(go_over_tip_commands)

            self.macroPipette(_action, _i)  # in microliters

            # move to clearance level on Z
            self.macroMoveSafeHeight()

        else:
            # TODO: discutir si es mejor descartar el tip
            #  si está automáticamente o tirar este error
            raise Exception(f"PROTOCOL ERROR: Cannot load or dispense without a tip. Action index: {_i}")

        return f"Processed macroGoToAndPipette action with index {_i}"


    def getTubeAndCoords(self, action, action_index):
        """Get XYZ coordinates of a tube and it's parameters.

        It can either get the info from a reference to a platform item and a content within,
        (i.e. platform-type) or directly from an XYZ coordinate (i.e. coord-type).

        Args:
            action (_type_): _description_

        Returns:
            _type_: _description_

        Example tube:      {'index': 2,
                            'maxVolume': 1500,
                            'name': 'buffer',
                            'position': {'col': 2, 'row': 1},
                            'tags': ['mixcomp', 'buffer'],
                            'type': 'tube',
                            'volume': 7.2}
        """
        if "coords" in action["args"]:
            # Get coords from a "coords"-type tube object.
            coords = action["args"]["coords"]
            _x, _y, _z = coords["x"], coords["y"], coords["z"]
            # Get information about the tube. It must contain "volume".
            _tube = action["args"]["tube"]
            # Ensure that the required data is there.
            _tube["volume"] = float(action["args"]["tube"]["volume"])
        else:
            # Get XYZ coordinates for the action's "platform"-type tube,
            # applying current tool offsets if any.
            _x, _y, _z = self.getTubeCoords(action)
            
            # Get tube (only a dict with a "volume" item is required).
            _tube = self.getTubeByAction(action)

        return _tube, _x, _y, _z

    def macroGoToAndMix(self, _action, _i, *ignored_args, **ignored_kwargs):
        """
        Homogenize solution.

        Possibly useful for rinsing before pipetting.

        Sample MIX action: {
            'args': {
                'count': 3,
                'item': '5x16_1.5_rack 1',
                'of': 'content',  # Can also be 'of': "tip"
                'percentage': 50,
                'coords': {"x": 123, "y": 123, "z": 123}, # Will override 'selector'
                'selector': {'by': 'name', 'value': 'tube1'}},
            'cmd': 'MIX'
        }

        Sample "tip":
            {
                "index": 1,
                "maxVolume": 160,
                "name": "tip1",
                "position": {
                "col": 1,  # Or: "x": 10?
                "row": 1   # Or: "y": 30?
                },
                "tags": [],
                "type": "tip",
                "volume": 0
            }

        Note that the tool must have been set by a previous tip pickup action.
        """

        # move to clearance level on Z
        # move to tube location on XY
        # move to Z at mixing height
        # load and dispense liquid, N times at speed.
        # flush tip
        # move to clearance level on Z

        if not self.tip_loaded:
            # TODO: discutir si es mejor descartar el tip
            #  si está automáticamente o tirar este error
            raise Exception(f"PROTOCOL ERROR: Cannot load or dispense without a tip. Action index: {_i}")

        # Move to clearance level on Z
        self.macroMoveSafeHeight()  # should extend task.commands list automatically

        _tube, _x, _y, _z = self.getTubeAndCoords(_action, _i)

        go_over_tip_commands = []
        # Move over the tip
        go_over_tip_commands.extend(gcode_primitives.gcodeMove(x=_x, y=_y, f=self.feedrate))
        # Go to the bottom of the tube
        go_over_tip_commands.extend(gcode_primitives.gcodeMove(z=_z, f=self.feedrate))
        # TODO: be smarter about pipetting height, using current tube volume to increase this.
        # Append commands
        self.commands.extend(go_over_tip_commands)
        self.current_action["GCODE"].extend(go_over_tip_commands)

        # Calculate volume limited to max pipette tool volume
        of = _action["args"]["of"]  # Either "tip" or tube "content".
        mix_percent = _action["args"]["percentage"]
        pipette_volume = self.pipettes[self.current_tool]['vol_max']
        if of == 'content':
            if self.verbose:
                logging.debug(f"gcodeBuilder message:    Action {_i}: mixing with tube volume percent {mix_percent}/100")
                message = [gcode_primitives.comment("MIX action by tube volume fraction.")]
            # Get tube volume
            # TODO: if the tube received volume during this protocol, is the updated volume available here? See: https://gitlab.com/pipettin-bot/pipettin-grbl/-/issues/109
            tube_volume = _tube["volume"]
            # Calculate mixing volume
            mix_vol = min(tube_volume * (mix_percent / 100), pipette_volume)
        elif of == 'tip':
            if self.verbose:
                logging.debug(f"gcodeBuilder message:    Action {_i}:" + f"Mixing with tip volume percent: {mix_percent}/100")
                message = [gcode_primitives.comment("MIX action by tip volume fraction.")]
            # Get tip volume
            tip_volume = self.tip_loaded['maxVolume']
            
            # TODO: would it make sense to check if a tip already has volume?
            # tip_volume -= self.tip_loaded.get("volume", 0)
            
            # Calculate mixing volume
            mix_vol = min(tip_volume * (mix_percent / 100), pipette_volume)
        else:
            raise Exception(f"PROTOCOL ERROR: Cannot parse mixing action with 'of'={of} at action index {_i}")

        if self.verbose:
            logging.debug(f"gcodeBuilder message:    Action {_i}: mixing with volume {mix_vol} uL.")
            message = [gcode_primitives.comment(f"MIX action with {mix_vol} uL.")]
            self.commands.extend(message)
            self.current_action["GCODE"].extend(message)

        # Prepare minimal pipetting action dict
        mix_count = int(_action["args"]["count"])

        # Do the mixing
        factor = 0.2 # fraction of volume that will be used for increasing mixing
        vol = mix_vol * (1 - factor)  # initial volume for mixing
        vol_increment = mix_vol * (factor / mix_count)
        for i in range(mix_count):
            # Load
            mix_action_up = {"args": {"volume": vol}}  # in microliters
            self.macroPipette(mix_action_up, _i, 
                              tip_priming_correction=False,
                              back_pour_correction=False,
                              capillary_correction=False)
            # Drop
            # The dropped volume will be greater than the loaded, to avoid
            # having unmixed solution remainder in the tip. In the final
            # iteration, the dropped volume will be the one in the action.
            vol += vol_increment
            mix_action_dw = {"args": {"volume": -vol}}  # in microliters
            self.macroPipette(mix_action_dw, _i, 
                              tip_priming_correction=False,
                              back_pour_correction=False,
                              capillary_correction=False)
            

        # Back-up
        # TODO: consider avoiding this if the same solution is pipetted next.
        self.macroMoveSafeHeight()

        return "Processed macroGoToAndMix action with index {_i}"

    def toolChangeMacro(self, new_tool:str, current_tool=None, return_commands=False):

        # Mark a toolchange / activate the new tool.
        if new_tool is not None:
            activate_tool_cmd = gcode_primitives.gcodeActivateTool(new_tool)
            self.commands.extend(activate_tool_cmd)
            self.current_action["GCODE"].extend(activate_tool_cmd)

        # Park current tool, if any.
        park_commands = []
        if current_tool is not None:
            safe_z = self.getSafeHeight()
            safe_y = self.getSafeY()

            # Prepare required arguments: "x, y, z, y_final, y_clearance, z_parking_offset, safe_z, safe_y, probe_name".
            # "tool_post": {"x": 316, "y": 276, "z": 45, "y_final": 312, "y_clearance": 110, "z_parking_offset": 0},
            tool_post_coords = deepcopy(self.pipette["tool_post"])

            # Build command
            park_commands = gcode_primitives.toolchange_probe(**tool_post_coords,
                                                             safe_z=safe_z, safe_y=safe_y,
                                                             extra_scan=1, closeup_dist=10,
                                                             feedrate=self.feedrate, 
                                                             safe_feedrate=600)
            # Add parking commands to the list.
            self.commands.extend(park_commands)
            self.current_action["GCODE"].extend(park_commands)

        else:
            # There is nothing to do if a tool is _not_ currently loaded.
            pass

        # Update the current tool.
        self.current_tool = new_tool
        # Update the current tool properties.
        if self.current_tool is None:
            # This should happen for a parking-only toolchange.
            self.pipette = None
        else:
            # And this for a normal toolchange
            self.pipette = self.pipettes[self.current_tool]  # save chosen micropipette model.

        # Pick-up the next tool
        pickup_commands = []
        if new_tool is not None:
            # Home tool axis
            self.macroPipetteHome(which_tool=new_tool)

            # Get the clearance coordinates
            safe_z = self.getSafeHeight()
            safe_y = self.getSafeY()
            
            # Example contents: "tool_post": {"x": 316, "y": 276, "z": 45, "y_final": 312, "y_clearance": 110, "z_parking_offset": 0},
            tool_post_coords = deepcopy(self.pipette["tool_post"])
            
            # Because we are picking up something, set the parking offset to zero.
            tool_post_coords["z_parking_offset"] = 0
            
            # Required arguments: "x, y, z, y_final, y_clearance, z_parking_offset, safe_z, safe_y".
            pickup_commands = gcode_primitives.toolchange_probe(**tool_post_coords,
                                                               safe_z=safe_z, safe_y=safe_y,
                                                               extra_scan=1, closeup_dist=10,
                                                               feedrate=self.feedrate,
                                                               safe_feedrate=600)
            # Add pickup commands to the list.
            self.commands.extend(pickup_commands)
            self.current_action["GCODE"].extend(pickup_commands)
        else:
            # There is nothing to do if the toolchange was just for parking.
            pass
        
        # Return
        if return_commands:
            return park_commands + pickup_commands
        else:
            return True

    #### Highl-level action parsers ####
    def parseAction(self, action, action_index=0):
        """This function interprets a protocol action and produces the corresponding GCODE

        Note that GCODE generation can depend on the current state.

        Args:
            action (dict): Python dictionary defining the action.
            action_index (integer): Action index number.

        Raises:
            Exception: Any exception is re-raised. Context info and traceback are printed just before.

        Returns:
            list: List of GCODE commands associated to the action.
        """        
        # Python switch case implementations https://data-flair.training/blogs/python-switch-case/
        # All action interpreting functions will take two arguments
        # action: the action object
        # i: the action index for the current action
        # task: the object of class "TaskEnvironment"
        
        # Assign current action to self slot.
        self.current_action = action
        # Create a list for the actions GCODE commands.
        self.current_action["GCODE"] = []

        try:
            # Get the command ID (e.g. "HOME" or "PICK_TIP").
            action_command = action['cmd']
            # Get the action's GCODE function.
            action_function = self.action_switch_case[action_command]

            # Register new action start as comment in the GCODE
            if self.verbose:
                message = [f" ; Building action {action_index}, with command: {action_command}"]
                self.commands.extend(message)
                self.current_action["GCODE"].extend(message)

            # Check for a toolchange
            self.toolchange = False
            if "args" in list(action):
                # Check for arguments in action
                args_tool = action["args"].get("tool", None)  # Either "str" or "None".
                if args_tool:
                    # Check for a tool specification.
                    # If the tool specification is different from the current one,
                    # a tool-change is needed before the action takes place.
                    if not args_tool.lower() == self.current_tool:
                        # Flag the tool-change, so it is prepended to the list of actions
                        self.toolchange = True  # self.current_tool = None
                        # TODO: Save state
                        # self.pipettes[self.current_tool]["current_volume"] = copy(self.state["vol"])

            # Append the tool-change command to the action commands if a toolchange is needed
            if self.toolchange:
                new_tool = args_tool.lower()
                self.toolChangeMacro(new_tool=new_tool, current_tool=self.current_tool)

            # Park the loaded tool if any, before final homing.
            # NOTE: The previous behaviour only parked tools before homing if this was the last action,
            #       this however depended on a protocol being defined, which might not be true for command streaming.
            # if (not self.toolchange) and (action_index + 1 == len(self.protocol["actions"])) and action_command == "HOME":
            if (not self.toolchange) and (action_command == "HOME"):
                if self.current_tool is not None:
                    self.toolChangeMacro(new_tool=None, current_tool=self.current_tool)

            # Produce the GCODE for the action.
            if self.verbose:
                logging.debug(f"parseAction: Processing command {action_command} with index {action_index}")
            # Use the action function to generate its GCODE.
            action_output = action_function(action, action_index)
            # Make a copy of the GCODE.
            action_commands = deepcopy(self.current_action["GCODE"])
        
        except Exception as err:
            msg = f"parseAction: parser error at action number {action_index} with content '{pprint.pformat()}' and message: {err}"
            logging.error(msg)
            print(msg)
            traceback.format_exc()
            raise Exception(msg)

        if self.verbose:
            msg = f"parseAction: Done processing action with command={action_command} index={action_index} and output={action_output}"
            logging.debug(msg)

        return action_commands

    def addAction(self, action, parse=True, append=True):
        
        # Default action index.
        action_index = 0
        # Add the new action to the protocol.
        if append:
            # Check that the protocol object has an actions key or create it.
            actions = self.protocol.setdefault("actions", [])
            # Append the action (note: this propagates to the action in the self.protocol dictionary).
            actions.append(action)
            action_index=len(actions)-1
        
        action_commands = None
        if parse:
            action_commands = self.parseAction(action=action, action_index=action_index)
        
        return action_commands

    def parseProtocol(self):
        """This function takes a task object and parses its actions into gcode"""

        # TODO: que los actions guarden su index en el dict, así no tengo que generarlos acá.
        protocol_parsed = [self.parseAction(action, i) for i, action in enumerate(self.protocol["actions"])]

    #### Output ####
    def save_gcode(self, path="examples/test.gcode"):
        """Save the GCODE to a file"""
        logging.debug(f"commanderTest.py message: Saving GCODE to file: {path}")
        with open(path, "w") as gcode_file:
            gcode_file.writelines(self.getGcode())

    def getGcode(self):
        return "\n".join(self.commands)
    
    def getActions(self):
        return self.protocol["actions"]
