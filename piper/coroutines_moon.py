# Third party imports.
import time
import asyncio
import aiohttp  # Imported even if unused explicitly, as it is required for HTTP requests by asyncio/websockets.
import json
from pprint import pprint, pformat
import traceback
import websockets
from hashlib import md5
import socketio
from copy import deepcopy
import sys
import importlib

# NOTE: Some errors may appear due to this https://stackoverflow.com/a/64690062
#       Similar to "NameError: name 'open' is not defined".
import logging
logging.basicConfig(filename='/tmp/moon.log',
                    #level=logging.DEBUG,  # More messages
                    level=logging.INFO,  # Less messages
                    filemode='w',
                    format='%(name)s - %(levelname)s - %(message)s')
logging.warning('Startup!')

# Internal imports.
from .commanders.sio_commander import SioCommander
from .commanders.klipper_commander import KlipperCommander
from .commanders.dummy import Dummy
from .gcode import GcodeBuilder
from .commander_utils import CustomFormatter

# create console handler with a higher log level
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
# set a format
ch.setFormatter(CustomFormatter())
# add the handler to the root logger
logging.getLogger('').addHandler(ch)


class Controller(SioCommander, KlipperCommander, Dummy):
    """A class to drive the Pipettin robot.

    Most of the functionality has been moved to the parent class, from which this inherits it's methods.
    """

    def __init__(self, *args, plugins=("example", "mongo"), config={}, **kwargs):
        # Save configuration.
        self.config = config
        self.verbose = kwargs.get("verbose", False)

        # Helper variables for asyncio coroutines.
        self.run = True
        self.background_task = None
        # List for the coroutines started by "moon_commander".
        self.coroutine_methods = []
        # List of asyncio tasks, setup by "gather" in moon_commander, and stopped elsewhere too.
        self.tasks = []

        # Instantiate a GcodeBuilder class.
        # This will track the state of the machine and generate GCODE.
        self.builder = GcodeBuilder(config=config, verbose=self.verbose)

        # Instantiate a database property (i.e. only MongoObjects for now).
        # This should be overwitten by plugins.
        self.database_tools = None

        # Loop over the requrested plugins, which must be the names of
        # python files (without ".py" extension), and "load" them into a dictonay property.
        self.plugins={}
        self.load_plugins(plugins)

        # Initialize the parent classes: https://stackoverflow.com/a/576183/11524079
        # The parent classes should be able to access the attributes set above through "self".
        super().__init__(*args, **kwargs)

    def load_plugins(self, plugins):
        """Load python modules from the 'plugins' directory through their 'load_controler' function."""
        for module_name in plugins:
            if self.verbose:
                logging.info("Controller: Importing plugin: " + str(module_name))
            # Load the requested module from the "plugins" sundirectory.
            module = importlib.import_module("piper.plugins." + module_name)
            # Save the module to the plugins list.
            self.plugins[module_name] = module.load_controler(self)

    # START methods section ####
    # TODO: write a generic start method that launches cotoutines or 
    #       task groups from the base class and the socketio coroutine.

    # START methods section ####
    def start(self):
        try:
            asyncio.run(self.moon_commander())
        except KeyboardInterrupt as e:
            logging.warn("Caught keyboard interrupt.")
            print("Caught keyboard interrupt.")
        # except asyncio.exceptions.CancelledError as e:
        #     print("start: A task was cancelled in the moon_commander: " + str(e))

    async def start_as_task(self):
        try:
            self.background_task = asyncio.create_task(self.moon_commander())
            await self.background_task
        except KeyboardInterrupt as e:
            logging.warn("Caught keyboard interrupt. Canceling tasks...")
            self.background_task.cancel()

    async def moon_commander(self):
        """
        Main co-routine, launching the other co-routines and catching any uncaught errors.
        """
        exit_status = True
        
        # Create a list of asyncio tasks.
        self.tasks = [asyncio.create_task(method) for method in self.coroutine_methods]
        
        # Schedule calls *concurrently*:
        try:
            await asyncio.gather(*self.tasks)
        
        except asyncio.exceptions.CancelledError as e:
            msg = "moon_commander: A task was cancelled in the moon_commander: " + str(e)
            logging.error(msg)
            print(msg)
            traceback.print_exc()
            exit_status = False
            # Signal coroutines to end
            self.ready = False
            self.run = False
            # End all tasks if at least one failed.
            for task in self.tasks:
                logging.error("Ending task: " + str(task))
                task.cancel()

        except Exception as e:
            msg = "moon_commander: Exceptions occured in the moon_commander tasks: " + str(e)
            logging.error(msg)
            print(msg)
            traceback.print_exc()
            exit_status = False
            # Signal coroutines to end
            self.ready = False
            self.run = False
            # End all tasks if at least one failed.
            for task in self.tasks:
                # https://stackoverflow.com/a/59655833
                task.cancel()
        else:
            msg = "moon_commander: coroutines gathered, stopping coroutines and closing sockets."
            logging.info(msg)
            print(msg)
        
        # Stop everything
        logging.info("moon_commander: stopping the controller and closing connections.")
        await self.stop()

        return exit_status

    # STOP/RESTART methods section ####
    # TODO: write a generic "stop" and "close" method.
    async def stop(self, timeout=2):
        """Stop the coroutines cleanly, or use the 'cancel' method if that failed.
        Also closes the socketio and websocket connections.
        This method is used by the pylabrobot backend to 'close' it at the end of a protocol.
        """
        # if timeout is None:
        #     timeout = self.min_interval*2.1
        
        logging.info("stop: Stopping task.")
        self.run = False
        # systemd.daemon.notify("STOPPING=1")

        # NOTE: Ready is only set by the websocket watchdog method.
        logging.info("stop: Setting ready to False.")
        self.ready = False

        exit_status = None, None

        # If started by start_as_task():
        if self.background_task is not None:
            logging.info("stop: Stopping background task.")
            elapsed = 0.0
            while not self.background_task.done():
                await asyncio.sleep(self.min_interval)
                elapsed += self.min_interval
                if elapsed >= timeout:
                    logging.warn("stop: Timed out.")
                    break
            
            if not self.background_task.done():
                logging.warn("stop: background_task not yet done. Cancelling coroutines.")
                await self.kill()
            else:
                logging.info("stop: Done.")
            
            exit_status = self.background_task.done(), self.background_task.cancelled()
        
        # If started by start():
        elif self.tasks:
            logging.info("stop: Stopping task list.")
            elapsed = 0.0
            while not self.task_list_done(self.tasks):
                await asyncio.sleep(self.min_interval)
                elapsed += self.min_interval
                if elapsed >= timeout:
                    logging.warn("stop: Timed out.")
                    break
            
            if not self.task_list_done(self.tasks):
                logging.warn("stop: tasks not yet done. Killing coroutines by cancel.")
                await self.kill()
            else:
                logging.info("stop: Done.")
            
            exit_status = self.task_list_done(self.tasks), self.task_list_cancelled(self.tasks)
        
        else:
            logging.info("stop: actions skipped. Coroutines were not setup as actions nor as background tasks.")

        # Close websocket and socketio connections.
        logging.info("stop: closing socket connections.")
        await self.close()

        return exit_status
    
    @staticmethod
    def task_list_done(task_list):
        return all([task.done() for task in task_list])

    @staticmethod
    def task_list_cancelled(task_list):
        return all([task.cancelled() for task in task_list])

    async def kill(self, timeout=None):
        """Kill the coroutines using the 'cancel' method."""
        if timeout is None:
            timeout = self.min_interval*2.1
        
        # If started by start():
        if self.background_task is not None:
            if not self.background_task.done():
                logging.info("kill: Canelling task.")
                self.background_task.cancel()
                
                elapsed = 0.0
                while not self.background_task.cancelled():
                    await asyncio.sleep(self.min_interval)
                    elapsed += self.min_interval
                    if elapsed >= timeout:
                        logging.warn("kill: Timed out")
                        break
            
            if self.background_task.done() or self.background_task.cancelled():
                logging.info("kill: Done.")

            return self.background_task.done(), self.background_task.cancelled()
        
        # Close websocket and socketio connections.
        elif self.tasks:
            logging.info("kill: Cancelling the tasks list.")
            for task in self.tasks:
                task.cancel()
            elapsed = 0.0
            while not self.task_list_cancelled(self.tasks):
                await asyncio.sleep(self.min_interval/4)
                elapsed += self.min_interval/4
                if elapsed >= timeout:
                    logging.warn("kill: Timed out")
                    break
            
            logging.info("kill: Done.")
            
            return self.task_list_done(self.tasks), self.task_list_cancelled(self.tasks)
        
        else:
            logging.info("kill: Nothing to kill")
            return True, True

    def killed(self):
        """Check on the status of the coroutines. Not exactly a 'kill' check."""
        # No longer works after switching from task_create() to asyncio.run().
        # return self.background_task.done() or self.background_task.cancelled()
        # TODO: find a way to check if the "asyncio.run" can be turned back to a task.
        # TODO: perhaps the "gather" call can be turned into a cancelable "task group".
        # NOTE: Removed "and self.printer_ready" because it blocked sending status
        #       request commands (to check for idle) if the printer was not ready.
        return not (self.run and self.ready)

    def killed_task(self):
        """Check on the status of the background async task."""
        if self.background_task:
            return self.background_task.done() or self.background_task.cancelled()
        elif self.tasks:
            return self.task_list_done(self.tasks), self.task_list_cancelled(self.tasks)
        else:
            logging.info("killed_task: warning, neither self.background_task nor self.tasks are defined.")
    
    def status(self):
        # TODO: add printer status.
        status = {
            "run": self.run,
            "killed": self.killed(),
            "ready": self.ready,
            "printer_ready": self.printer_ready
        }

        if self.background_task:
            status.update({
                "background_task": True,
                "background_task.done": self.background_task.done(),
                "background_task.cancelled": self.background_task.cancelled()
            })
        else:
            status.update({
                "background_task": self.background_task
            })

        # Let parent classes rport status as well.
        status.update(
            super().status()
        )
        
        return status
    
    async def close(self, timeout=3):
        # Call the "close" method from the parent of the 'Controller' class.
        # Read: https://www.geeksforgeeks.org/multiple-inheritance-in-python/
        logging.info("Closing controller.")
        await super().close(timeout)

    async def test(self):
        # Call the "close" method from the parent of the 'Controller' class.
        # Read: https://www.geeksforgeeks.org/multiple-inheritance-in-python/
        await super().test()


if __name__ == '__main__':
    # This module can be called from the CLI from the module's main directory.
    # You can set a value to an empty string by omitting the left hand side.
    # For example, run:
    #   python3 -m piper.coroutines_moon dry verbose ws_address=''

    kw_dict = {}
    for arg in sys.argv[1:]:
        if '=' in arg:
            sep = arg.find('=')
            key, value = arg[:sep], arg[sep + 1:]
            kw_dict[key] = value
        else:
            kw_dict[arg] = True
    
    logging.info("coroutines_moon: starting up with arguments" + pformat(kw_dict))
    try:
        moon_kontrol = Controller(**kw_dict)
        if kw_dict.get("verbose", False):
            asyncio.run(moon_kontrol.test())
        moon_kontrol.start()
    except Exception as e:
        msg = "coroutines_moon: caught exception with message: " + str(e)
        logging.error(msg)
        print(msg)
        traceback.print_exc()
    else:
        logging.info("coroutines_moon: the coroutines have ended.")
    
    try:
        logging.info("coroutines_moon: stopping the controller and closing connections.")
        asyncio.run(moon_kontrol.stop())
    except Exception as e:
        print("Could not stop controller: " + str(e))
