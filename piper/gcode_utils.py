from time import sleep
from math import ceil, floor, pi
import traceback
from pprint import pprint

# try:
#     from RPi import GPIO
# except ImportError:
#     print("Dummy GPIO")
#     from dummyGPIO import GPIO
#
# try:
#     import pigpio as pigpio
# except ImportError:
#     print("Dummy pigpio")
#     from dummyGPIO import pigpio

LIMITS = {"z_max": 258,  # Z is homing at the top, with positive direction upwards.
          "x_max": 350,  # X and Y are homing at 0 and move in positive space.
          "y_max": 320,
          "z_min": 0,
          "x_min": 0,
          "y_min": 0
          }

# Available micropipette models and characteristics
PIPETTES = {
    "p200": {
        # Physical parameters
        # These defaults are also defined in gcodeBuilder.py
        # Según eso, el desplazamiento máximo del émbolo es 15.91 mm.
        "vol_max": 170,                         # [microliters] maximum pipette volume (as indicated by dial) with some tolerance subtracted.
        "scaler": pi*((4/2)**2),                # [microliters/millimeter] conversion factor, from mm (shaft displacement) to uL (corresponding volume).
        "scaler_adjust": 1.00,                  # [ratio] adjustment factor, from shaft volume (uL) to tip volume (uL).
        "tipLength": 50,                        # [millimeters] total length of the pipette's tip.
        "tipSealDistance": 6,                   # [millimeters] distance the pipette must enter the tip to properly place it.
        "tip_probing_distance": 12,             # [millimeters] Clearance distance before probing
        "probe_extra_dist": 5,                  # [millimeters] Extra distance to probe then placing tips, beyond the "defaultLoadBottom[tool_name]" platform value.
        "backlash_compensation_volume": 0.1,    # [microliters] extra move before poring volume, corresponds to backlash error after drawing (only applies after a direction change).
        "extra_draw_volume": 8,                 # [microliters] extra volume that the pipette needs to draw (to avoid the "under-drawing" problem). See: calibration/data/21-08-17-p200-balanza_robada/README.md
        "back_pour_correction": 4,              # [microliters] volume that is returned to the source tube after pipetting into a new tip (a sort of backlash correction).
        "under_draw_correction": 4,           # [microliters] volume that is returned to the source tube after pipetting into a new tip (a sort of backlash correction).
        "current_volume": None,
        "retraction_displacement": 20.0,        # [millimeters] Downward displacement after hitting the limit switch, meant to push the pipettes shaft precisely up to its first stop.
        # Coordinates and offsets
        # Pipette tip offset, set to zero on the first tool (by definition).
        "tool_offset": {"x": 0, "y": 0, "z": 0, "z_bare": 0, "z_tip": 0},
        "tool_post": {"x": 320.5, "y": 266, "z": 17,
                      "y_final": 297, "y_clearance": 110, "z_parking_offset": 2,
                      # Probe object name for Klipper MULTIPROBE.
                      "probe_name": "p200_probe"},
        "eject_post": {"post_x": 185, "post_y": 301,
                       "post_z_pre": 63, "post_z": 73,
                       "eject_feed_rate": 600},
        # State tracking
        "state": {"s": 0, "vol": 0,  # pipette axis position in mm (s) and microliters (vol).
                  "tip_vol": 0,
                  "lastdir": None,
                  "x": None,
                  "y": None,
                  "z": None,
                  "platform": None,
                  "tube": None,
                  "paused": False,
                  "alarm": False
                  },
        # Low-volume capillary "correction"
        "tension_correction": {
            "start_vol": 20,
            "max_correction": 2
        }
    },
    "p20": {
        # Physical parameters
        # These defaults are also defined in gcodeBuilder.py
        "vol_max": 19,                          # [microliters] maximum pipette volume (as indicated by dial) with some tolerance subtracted.
        # No pude medir con precisión, dio 1.30 mm de diametro.
        # El desplazamiento del émbolo es 15.7 mm (+/- 0.02 mm).
        # Esa cuenta da 20.8 uL por desplazamiento, es razonable.
        "scaler": pi*((1.30/2)**2),             # [microliters/millimeter] conversion factor, from mm (shaft displacement) to uL (corresponding shaft volume).
        "scaler_adjust": 1.08,                  # [ratio] adjustment factor, from shaft volume (uL) to tip volume (uL). Calibrated.
        "tipLength": 50,                        # [millimeters] total length of the pipette's tip.
        "tipSealDistance": 6,                   # [millimeters] distance the pipette must enter the tip to properly place it.
        "tip_probing_distance": 12,             # [millimeters] Clearance distance before probing
        "probe_extra_dist": 7,                  # [millimeters] Extra distance to probe then placing tips, beyond the "defaultLoadBottom[tool_name]" platform value.
        "backlash_compensation_volume": 0.1,    # [microliters] extra move before poring volume, corresponds to backlash error after drawing (only applies after a direction change).
        "extra_draw_volume": 20*0.04,           # [microliters] extra volume that the pipette needs to draw (to avoid the "under-drawing" problem). See: calibration/data/21-08-17-p200-balanza_robada/README.md
        "back_pour_correction": 0.7,            # [microliters] volume that is returned to the source tube after pipetting into a new tip (a sort of backlash correction).
        "under_draw_correction": 0.5,           # [microliters] volume that is returned to the source tube after pipetting into a new tip (a sort of backlash correction).
        "current_volume": None,
        "retraction_displacement": 20.5,        # [millimeters] Downward displacement after hitting the limit switch, meant to push the pipettes shaft precisely up to its first stop.
        # Coordinates and offsets
        # Pipette tip offset, relative to the offset in the first tool.
        "tool_offset": {
            # XY offset, calculated as the difference of equivalent positions (e.g. over the exact same physical tip).
            # The difference is calculated between the reference pipette and this pipette (e.g. xy_offset = p20_xy_coords - p200_xy_ref_coords).
            "x": 2, "y": 3.25,
            # Z offset between the reference pipette and this pipette,
            # when no tips are placed on neither pipette.
            "z_bare": -6,
            # Z offset between the reference pipette and this pipette,
            # when tips _are_ placed on both pipettes.
            "z_tip": 9.8,
            "z": 9.8
            },
        "tool_post": {"x": 19, "y": 266, "z": 20,
                      "y_final": 298.5, "y_clearance": 110, "z_parking_offset": 1,
                      # Probe object name for Klipper MULTIPROBE.
                      "probe_name": "p20_probe"},
        "eject_post": {"post_x": 185, "post_y": 301,
                       "post_z_pre": 64, "post_z": 73,
                       "eject_feed_rate": 600},
        # State tracking
        "state": {"s": 0, "vol": 0,  # pipette axis position in mm (s) and microliters (vol).
                  "tip_vol": 0,
                  "lastdir": None,
                  "x": None,
                  "y": None,
                  "z": None,
                  "platform": None,
                  "tube": None,
                  "paused": False,
                  "alarm": False
                  },
        # Low-volume capillary "correction"
        "tension_correction": {
            "start_vol": 2,
            "max_correction": 0.2
        }
    }
}
