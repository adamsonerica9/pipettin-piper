# Reference commands at: https://moonraker.readthedocs.io/en/latest/web_api/

def printer_info(info_command_id):
    # rpc_primitives.printer_info
    return {"jsonrpc": "2.0", "method": "printer.info", "id": info_command_id}

def emergency_stop(id=42):
    # rpc_primitives.emergency_stop
    return {"jsonrpc": "2.0","method": "printer.emergency_stop","id": id}

def firmware_restart(id=8463):
    # rpc_primitives.firmware_restart
    return { "jsonrpc": "2.0", "method": "printer.firmware_restart", "id": id}

def gcode_script(gcode_cmd, cmd_id):
    # rpc_primitives.gcode_script
    return {
        "jsonrpc": "2.0", 
        "method": "printer.gcode.script", 
        "params": {"script": gcode_cmd}, "id": cmd_id
    }

def gcode_help(id=4645):
    return {"jsonrpc": "2.0", "method": "printer.gcode.help", "id": id}

def query_objects(objects, id=4654):
    """Query Klipper printer objects.

    See: https://moonraker.readthedocs.io/en/latest/printer_objects/
    
    Args:
        objects (_type_): _description_
        id (int, optional): _description_. Defaults to 4654.

    Example:
        objects = {
            "idle_timeout": None,  # A 'null' value will fetch all available attributes for its key.
            "toolhead": ["position", "status"]
        }

    Returns:
        _type_: _description_
    """
    return  {"jsonrpc": "2.0", "method": "printer.objects.query", "params": {"objects": objects}, "id": id}

def query_idle_timeout(id=4654):
    """The 'idle_timeout' object reports the idle state of the printer.

    See: https://moonraker.readthedocs.io/en/latest/printer_objects/#idle_timeout

    Example response:
        {
            "state": "Idle",
            "printing_time": 0.0
        }

    state: Can be Idle, Ready, or Printing. The printer will transition to the Printing state whenever a gcode is issued that commands the tool, 
    this includes manual commands. Thus this should not be used to determine if a gcode file print is in progress. It can however be used to determine if the printer is busy.

    Args:
        id (int, optional): _description_. Defaults to 4654.
    """

    objects = {"idle_timeout": None}  # A 'null' value will fetch all available attributes for its key.
    return query_objects(objects=objects, id=id)
