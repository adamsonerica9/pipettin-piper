# GUI command parser.

# Third-party imports
import logging, json, traceback, re
from pprint import pformat, pprint

# Internal imports
from ..gcode import GcodeBuilder
from .mongo import MongoObjects
from .. import gcode_primitives

class CommandRouter:
    """
    Receives a command from the GUI/backend and generates GCODE.

    This class is made to parse commands from the GUI (i.e. from the socket.io connection) into executable GCODE.
    The GCODE will likely not be standard. This depends on the particular implementation of "gcode primitives".

    TO-DO:
        - A plugin system is on the works. Plugins are expected to add keys and methods to the 'self.commands' dictionary.

    Examples of event data from the GUI:

        XYZ home:      ["p2g_command", {"home": "xyz"}]
        Tool home:     ["p2g_command", {"home": "p", 
                                        "tool": "P200"}]
        ALL home:      ["p2g_command", {"home": "all"}]
        
        Move X/Y/Z:    ["p2g_command", {"move": "all", 
                                        "move_direction": "x", 
                                        "distance": 20}]
        Move E:        ["p2g_command", {"move": "all", 
                                        "move_direction": "e", 
                                        "distance": 20, 
                                        "tool": "P200"}]
        
        GCODE:         ["p2g_command", {"command": ["G0 X10", "FIRMWARE_RESTART"]}]
        
        Go-to:         ["p2g_command", {"calibration": "goto", 
                                        "workspace": "calibracion_plataformas", 
                                        "item": "5x16_1.5_rack 1", 
                                        "content": "tube1"}]
        
        Run protocol:  ["p2g_command", {"run-protocol":"prueba 2023-01-14T08:40:00.259Z", 
                                        "mongo_url": "mongodb://localhost:27017/"}]

    Parameter descriptions:

    - mongo_url     : URL to the MongoDB server.
    - mongo_db      : Name of the databse in MongoDB.
    - home          : If set to a valid string, the specified machine's axis homed (i.e. "xyz").
    - calibration   : If set to a valid string, it performs a "callibration" type move (i.e. a "goto" callibration command).
    - run-protocol  : If set to a valid protocol name, it runs a protocol. It must match the name of a protocol in MongoDB.
    - move          : If set to a valid string, it indicates a "move" type command ("jogging" in CNC jargon).
    - command       : If set to a string with a valid GCODE comand, it is passed directly to the machine's controller.
    - distance      : Distance relevant to the command, if any (i.e. for the "move" command).
    - workspace     : Workspace name for the protocol.
    - item          : Name of the relevant platform item in the workspace, if any (i.e. for the "move" or "callibration" commands).
    - content       : Name of the relevant platform content in the workspace, if any (i.e. for the "move" or "callibration" commands).
    - tool          : Name of tool to use.
    - verbose       : If True, a lot of messages will be printed to the console.
    """
    def __init__(self,
                 controller,
                 gcode_builder: GcodeBuilder = None, 
                 database_tools: MongoObjects = None,
                 config = {},
                 verbose = True):

        # Save configuration.
        self.config = config
        # Save controller.
        self.controller = controller

        # Save database tools.
        self.database_tools=database_tools
        
        # Names/IDs of the available commands.
        # Data that contains
        self.commands = {
            "home": self.make_home_commands,
            "move": self.make_move_commands,
            "gcode": self.raw_gcode_command,
            "goto": self.make_goto_command,
            "run-protocol": self.make_protocol_commands
        }
        self.verbose = verbose
        
        # Save the GcodeBuilder object.
        if gcode_builder:
            self.builder = gcode_builder
        else:
            self.builder = GcodeBuilder()

        # Register default socketio event handlers.
        self.sio = controller.sio
        self.register_sio_callbacks()

    # SOCKET.IO CALLBACKS AND EVENTS SECTION ####
    def register_sio_callbacks(self):
        """Function to register socketio event callbacks, typically sent by the Pipettin GUI."""
        if self.sio:
            @self.sio.on('p2g_command')
            async def command_message_handler(data):
                """
                Receives a command from the GUI/backend and sends them to the GCODE generator.
                """
                logging.info(f"command_message_handler: received p2g command with data: {pformat(data)}")

                try:
                    # Parse the GUI command to GCODE.
                    # gcode = command_router(gui_command=data,
                    #                        commander=self,
                    #                        verbose=self.verbose)
                    # NOTE: replaced old "command_router" with this "plugin".
                    gcode = self.parse_command(data)

                    # Exit if dry run is active
                    if self.controller.dry:
                        logging.info("p2g_command: commands skipped, 'dry' mode is enabled. Output gcode:\n" + pformat(gcode))
                        return True
                    
                    # Use a method from KlipperCommander to convert the
                    # commands to RPC form and send them to Moonraker.
                    await self.controller.run_gcode_protocol(gcode)
                
                except Exception as e:
                    msg = "p2g_command: caught exception with message: " + str(e)
                    logging.error(msg)
                    print(msg)
                    traceback.print_exc()
                    return False

        # @staticmethod
        @self.sio.on('kill_commander')
        async def command_kill_handler(data):
            logging.info("command_message_handler: received 'kill_commander' command: " + pformat(data))
            if self.verbose: print("command_message_handler: received 'kill_commander' command: ", pformat(data))

            # Send "emergency_stop" message to Klipper/Moonraker.
            await self.emergency_stop()

            # Stop/kill coroutines
            await self.stop()

        # @staticmethod
        @self.sio.on('go_to')
        def command_go_to(data):
            logging.info("command_message_handler: received 'go_to' socketio command: " + str(data))
            pass


    def parse_command(self, data: dict, only_first=True):
        """Send the data to the appropriate method, looking for the method's ID in the data's keys."""
        gcode = None
        for command_name in self.commands.keys():
            if command_name in data.keys():
                # Get the method associated to the command name and generate the GCODE.
                gcode = self.commands[command_name](data)
                if only_first:
                    # The data may (in aberrant cases) have content matching multiple command IDs.
                    # So, just in case, run only the first match.
                    break
        return gcode
    
    # GCODE generators.
    def make_home_commands(self, data):
        home_sweet_home = []
        home_tools_cmd = []

        if self.verbose:
            logging.info(f"command_router message: sending home command: {data['home']}")

        # XYZ axes homing
        pattern = re.compile(".?[xyz]")
        if pattern.match(data['home']) is not None:
            # Enable steppers
            # commander.steppers_on()
            # Build gcode
            home_sweet_home = gcode_primitives.gcodeHomeXYZ(which_axis=data['home'])

        # Tool homing
        if data['home'].upper().find("P") > -1 or data['home'].upper() == "ALL":
            if self.verbose:
                logging.info("command_router: Sending pipette axis home...")
            # Home
            if data['tool'] != "":
                home_tools_cmd = gcode_primitives.gcodeHomeP(which_axis=data['tool'].upper())
            else:
                home_tools_cmd = gcode_primitives.gcodeHomeP()

        # Return homing commands
        return home_sweet_home + home_tools_cmd

    def make_move_commands(self, data):
        # Get move direction from arguments
        move_direction = str(data['move']).lower()

        # XYZ movements
        if move_direction in ["x", "y", "z"]:
            # Build gcode
            move_params = {"x": None, "y": None, "z": None}
            move_params[move_direction] = data['distance']
            move_command = gcode_primitives.gcodeMove(absolute=False,
                                                     x=move_params["x"],
                                                     y=move_params["y"],
                                                     z=move_params["z"])

        # Tool movements
        elif move_direction == "p":
            if self.verbose:
                logging.info(f"command_router message: Pipette move: dist={data['distance']}, T={data['tool']}")

            # Configure pipette if specified
            tool_activate_command = []
            if data['tool'] != "":
                tool = data['tool'].upper()
                tool_activate_command = gcode_primitives.gcodeT(tool)

            # Build move command
            move_command = tool_activate_command + gcode_primitives.gcodeMove(absolute=False, e=data['distance'])

        # Return commands
        return move_command

    def raw_gcode_command(self, data):
        if self.verbose:
            logging.info(f"command_router message: sending raw command: {data['command']}")
        
        # Return commands
        return data["command"]
    
    def make_goto_command(self, data):
        if self.verbose:
            logging.info(f"command_router message: sending 'goto' command as fast_grbl_command: {data['calibration']}")
            logging.info(f"    Using workspace: {data['workspace']}")
            logging.info(f"    Using item: {data['item']}")
            logging.info(f"    Over content: {data['content']}")
            logging.info(f"    With tool: {data['tool']}")

        # Generate move command
        # TODO: check if other parts of the code expected a string from "data['content']".
        command, message = self.database_tools.makeGoToPlatformCommand(workspace_name=data['workspace'],
                                                                       platform_name=data['item'],
                                                                       content_name=data['content']["name"],
                                                                       tool_name=data['tool'])
        # Return commands
        return command

    def make_protocol_commands(self, data):
        if self.verbose:
            logging.info(f"command_router message: Provided protocol name: '{data['run-protocol']}'")
            logging.info("command_router message: Loading protocol objects from MONGODB.")

        # Load protocol objects from mongodb:
        protocol, workspace, platforms_in_workspace = self.database_tools.getProtocolObjects(protocol_name=data['run-protocol'])

        if self.verbose:
            logging.info("command_router message: Instantiating protocol task class with protocol:\n" + pformat(protocol))
            # logging.info(json.dumps(protocol, indent=2, default=str))  # https://stackoverflow.com/a/56061155/11524079

        # Initiate the task class, which will hold all relevant information for the current task.
        # builder = self.builder(protocol, workspace, platforms_in_workspace)  # NOTE: now the object is inherited.
        
        # Update objects in the task class, which will hold all relevant information for the current task.
        self.builder.initialize_objects(protocol, workspace, platforms_in_workspace)

        # TODO: Pipette configs should be copied.
        # builder.pipettes = commander.pipette.pipettes

        # Generate GCODE for the task, it is saved in the task class object.
        self.builder.parseProtocol()

        # Print it
        if self.verbose:
            logging.debug("command_router: GCODE commands generated: " + pformat(self.builder.commands))

        # Get the command list, and pass it on to the commander.
        # if not args.dry:
        #     await commander.run_gcode_protocol(commands=builder.commands)
        
        # TODO: migrate this functionality back to moonControl.
        protocol_file_path = "/tmp/protocol.gcode"
        self.controller.save_gcode_protocol(gcode_commands=self.controller.builder.commands, file=protocol_file_path)
        # Save GCODE to a file in Klipper's printer_data
        if self.verbose:
            logging.info("command_router message: Saved protocol to file " + protocol_file_path)
        
        # Since coroutines_moon will execute any returned GCODE commands,
        # if we want to run the GCODE manually we must return an empty list instead.
        # return []
        # Return commands
        return self.builder.commands

def load_listener(controller):
    return CommandRouter(
        controller, 
        gcode_builder=controller.builder, 
        config=controller.config,
        database_tools=controller.database_tools)
