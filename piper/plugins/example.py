# Dummy piper plugin.
import logging
from pprint import pformat

# Import socketio, for the add handler example.
from socketio import AsyncClient

# Controller extension.
class ExampleController:
    def __init__(self, controller, **kwargs) -> None:
        self.controller = controller
        self.kwargs: dict = kwargs
        logging.info("ExampleController: loaded with options: " + pformat(kwargs))

        # Example: add socket.io handlers.
        self.sio: AsyncClient = controller.sio
        self.add_sio_handlers()

    def add_sio_handlers(self):
        """ Add event handlers to the socket.io object from the controller class.
        The events will typically originate from the Pipettin Writer GUI's backend.
        """

        # Register an event handler. This uses the socket.io object from the controller class.
        @self.sio.on('example_event')
        async def example_event_handler(data: dict):
            # Log the event.
            logging.info("ExampleController: received 'example_event' command: " + pformat(data))
            
            # Do something with the data...
            id = data.get("id", None)  # Get the ID property, returning "-1" if not found.

            # Respond to the event.
            return {"id": id, "data": "example response data"}

        @self.sio.on('*')
        def catch_all(data=None):
            logging.info(f"ExampleController: received uncaugh event from socketio with data: " + pformat(data))


def load_controler(controller, **kwargs):
    """
    Controller plugins are expected to have a function named 'load_controler' which will instantiate
    the plugin's class by passing it the instance of the main controller class (coroutines_moon.moonControl),
    and finally returning the class instance.
    """
    class_instance = ExampleController(controller, **kwargs)
    return class_instance

# Action slicer (GCODE generators) extension.
class ExampleGcoder:
    def __init__(self, gcode_builder, **kwargs) -> None:
        self.gcode_builder = gcode_builder
        self.kwargs = kwargs
        self.action_name = "EXAMPLE"

def load_gcoder(gcode_builder, **kwargs):
    """
    GCODE plugins are expected to have a function named 'load_gcoder' which will instantiate
    the plugin's class by passing it the instance of the main gcode class (gcode.GcodeBuilder),
    and finally returning the class instance.
    """
    class_instance = ExampleGcoder(gcode_builder, **kwargs)
    return class_instance
