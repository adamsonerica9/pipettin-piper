# Piper Plugins

## Controller plugins

These plugins can act on the piper controller class `moonControl` located at [coroutines_moon.py](../coroutines_moon.py).

For instance, the [example.py](./example.py) shows how to catch and respond to socket events received by Piper (typically sent by the Pipettin Writer web UI).

## GCODE generator plugins

These plugins can interact with the `GcodeBuilder` class, defined at [gcode.py](../gcode.py).

Their main purpose is to add new action handlers to the `GcodeBuilder.action_switch_case` attribute, supporting additional funcionality.

# TO-DO

- [ ] Allow new non-GCODE actions to be handled differently than RPC commands for Klipper. Ideally, Klipper can be separated from the main code and set as just another command interpreter.
- [ ] Use the new [coorder](../../../../extra/coorder) classes to send commands. Websocket command management for Klipper/Moonraker should be moved over here.
