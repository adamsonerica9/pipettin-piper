import pymongo, logging
from .. import gcode_primitives
from ..gcode_utils import PIPETTES

class MongoObjects(object):
    """A class holding machine current status or configuration, a class for machine state tracking."""
    """Also holds all GCODE commands after parsing protocols."""

    def __init__(self,
                 mongo_url='mongodb://localhost:27017/',
                 database_name='pipettin',
                 pipettes=None,
                 parser_object=None, verbose=True, pipette=None):

        self.parser_object = parser_object
        self.verbose = verbose
        self.mongo_url = mongo_url

        # Save the available pipettes.
        if pipettes:
            self.pipettes = pipettes
        else:
            self.pipettes = PIPETTES  # TODO: replace this with DB collection.
        
        if pipette is None:
            self.pipette = self.pipettes[list(self.pipettes)[0]]
            if self.verbose:
                default_pipette_name = list(self.pipettes)[0]
                logging.info(f"MongoObjects message: defaulting to the first pipette: {default_pipette_name}")

        # DATABASE SETUP ############

        # connect and select database name
        if self.verbose:
            logging.info("MongoObjects message: Connecting to mongodb")
        self.client = pymongo.MongoClient(self.mongo_url)
        self.db = self.client[database_name]

        # collection names
        self.collectionProtocols = self.db['protocols']
        self.collectionWorkspaces = self.db['workspaces']
        self.collectionPlatforms = self.db['platforms']

        # ensure indexes
        self.collectionProtocols.create_index([('name', pymongo.ASCENDING)], unique=True)
        self.collectionWorkspaces.create_index([('name', pymongo.ASCENDING)], unique=True)
        self.collectionPlatforms.create_index([('name', pymongo.ASCENDING)], unique=True)

    def makeGoToPlatformCommand(self, workspace_name, platform_name, content_name, tool_name):
        """
        Annoying function to calculate platform position.
        Should be replaced with something from the TaskClass stuff.

        TODO: replace z logic with new tool offset logic.
        """

        logging.debug(f"makeGoToPlatformCommand: running with workspace_name={workspace_name}, platform_name={platform_name}, content_name={content_name}, tool_name={tool_name}")

        # Get workspace object
        workspace = self.collectionWorkspaces.find_one({"name": workspace_name})
        if not workspace:
            if self.parser_object is None:
                raise Exception("MongoObjects.makeGoToPlatformCommand: protocol's workspace not found.")
            else:
                self.parser_object.error("protocol's workspace not found.")

        # Get platform names in workspace
        platform_names = list(map(lambda p: p['platform'], workspace['items']))

        # Get platforms in workspace
        platforms_in_workspace = list(self.collectionPlatforms.find({"name": {"$in": platform_names}}))

        platform_item = self.getWorkspaceItemByName(workspace=workspace, platform_name=platform_name)

        platform = self.getPlatformByName(platformsInWorkspace=platforms_in_workspace, platform_item=platform_item)

        content = self.getContentByName(content_name=content_name, platform_item=platform_item)

        # Calculate XY of tube center
        _x = platform_item["position"]["x"]
        _x += platform['firstWellCenterX']
        _x += (content["position"]["col"] - 1) * platform['wellSeparationX']

        _y = platform_item["position"]["y"]
        _y += platform['firstWellCenterY']
        _y += (content["position"]["row"] - 1) * platform['wellSeparationY']
        
        # Add X tool offset
        if tool_name is not None:
            _x += self.pipettes[tool_name.lower()]["tool_offset"]["x"]
            _y += self.pipettes[tool_name.lower()]["tool_offset"]["y"]

        if self.verbose:
            logging.info(f"MongoObjects.makeGoToPlatformCommand message: Moving to position X={_x} Y={_y} and platform {platform}")

        command = gcode_primitives.gcodeMove(x=_x, y=_y, absolute=True)

        # Calculate Z for pipetting #########################################################
        # TODO: check and tune Z positioning according to tip seal pressure or seal distance,
        #  this might need calibration. Ver issue #7 y #25
        # https://github.com/naikymen/pipettin-grbl/issues/7
        # https://github.com/naikymen/pipettin-grbl/issues/25

        # First reference is the "bottom" of the tube.
        default_bottom_position = platform["defaultBottomPosition"]
        _z = default_bottom_position

        # Prepare message for the socket
        message = f"Default pipetting position for the platform is: {_z}"  # + "\n"

        # Increase height because a tip is placed
        # TODO: replace with offset logic.
        if self.pipette is not None:
            tip_length = self.pipette["tipLength"]  # p200 default is 50
            _z += tip_length
            message += ". The tip length is: " + str(tip_length)

        # Decrease height because the pipette sinks into the tip to make the "seal"
        # TODO: replace with offset logic.
        if self.pipette is not None:
            tip_seal_distance = self.pipette["tipSealDistance"]  # p200 default is 6
            _z += -tip_seal_distance  # TO-DO: how should this be callibrated?
            message += ". The tip sealing distance is: " + str(tip_seal_distance)

        message += ". The final pipetting Z position would be: " + str(_z)
        message += "\n\n"
        message += "You might want to move the Z axis manually to that position, with a tip placed, "
        message += "and check if the tip is about 1 mm above the bottom of the tube. "
        message += "That is a good height for small volumes (20-200 ul). At least 20 uL must remain in the tube."

        return command, message

    def listProtocols(self):
        """
        Annoying function to get protocols from MongoDB as a list, log protocol names, and return them.
        """
        protocols = list(self.collectionProtocols.find())

        if self.verbose:
            logging.info(f"MongoObjects.listProtocols: Found the following protocols: {[p['name'] for p in protocols]}")

        return protocols

    def listWorkspaces(self):
        """
        Annoying function to get workspaces from MongoDB as a list, log names, and return them.
        """
        workspaces = list(self.collectionWorkspaces.find())

        if self.verbose:
            logging.info(f"\nMongoObjects.listWorkspaces: Found the following workspaces: {[w['name'] for w in workspaces]}")

        return workspaces

    def listPlatforms(self):
        """
        Annoying function to get platforms from MongoDB as a list, log names, and return them.
        """
        platforms = list(self.collectionPlatforms.find())

        if self.verbose:
            logging.info(f"MongoObjects.listPlatforms: Found the following platforms: {[pl['name'] for pl in platforms]}")

        return platforms

    def getProtocolObjects(self, protocol_name):
        """
        Annoying function to make protocol gcode.
        Should be moved into the task class thing.
        """

        # ARGUMENT CHECKS ############

        # arg is required
        if type(protocol_name) is not str:
            if self.parser_object is None:
                raise Exception("MongoObjects.getProtocolObjects: run-protocol is required.")
            else:
                self.parser_object.error("run-protocol is required.")

        # get protocol data by name
        protocol = self.getProtocolByName(protocol_name=protocol_name)

        # get protocol's workspace data
        workspace = self.getWorkspaceByName(workspace_name = protocol['workspace'])
        
        # DATABASE QUERIES ############

        # get platforms in workspace
        platforms_in_workspace = self.getPlatformsInWorkspace(workspace)

        # return all
        return protocol, workspace, platforms_in_workspace

    def getPlatformsInWorkspace(self, workspace):
        # extract platform names in workspace
        platform_names = list(map(lambda p: p['platform'], workspace['items']))
        # get platforms in workspace
        platforms_in_workspace = list(self.collectionPlatforms.find({"name": {"$in": platform_names}}))
        # done
        return platforms_in_workspace

    def getWorkspaceByName(self, workspace_name: str):
        # get protocol's workspace data
        workspace = self.collectionWorkspaces.find_one({"name": workspace_name})
        if not workspace:
            if self.parser_object is None:
                raise Exception("MongoObjects.getProtocolObjects: protocol's workspace not found.")
            else:
                self.parser_object.error("MongoObjects.getProtocolObjects: protocol's workspace not found.")
        return workspace

    def getProtocolByName(self, protocol_name: str):
        # get protocol data by name
        protocol = self.collectionProtocols.find_one({"name": protocol_name})
        if not protocol:
            if self.parser_object is None:
                raise Exception("MongoObjects.getProtocolObjects: protocol not found.")
            else:
                self.parser_object.error("MongoObjects.getProtocolObjects: protocol not found.")
        return protocol

    @staticmethod
    def getWorkspaceItemByName(workspace, platform_name):
        """Iterate over items in the workspace looking for one who's name matches 'platform_name' """
        for item in workspace["items"]:
            if item["name"] == platform_name:
                return item
            else:
                continue
        return None

    @staticmethod
    def getPlatformByName(platformsInWorkspace, platform_item):
        """Iterate over platforms in workspace looking for one who's name matches the platform in 'platform_item' """
        for platform in platformsInWorkspace:
            if platform["name"] == platform_item["platform"]:
                return platform
            else:
                continue
        return None

    @staticmethod
    def getContentByName(content_name, platform_item):
        for content in platform_item["content"]:
            if content['name'] == content_name:
                return content
            else:
                continue
        return None

def load_controler(controller):
    if controller.database_tools:
        raise Exception("MongoObjects: controller.database_tools already set to: " + str(controller.database_tools))
    # Set the databse_tools property to an instance of MongoObjects.
    # TODO: pass remaining configuration here.
    controller.database_tools = MongoObjects(verbose=controller.verbose)
    return controller.database_tools
