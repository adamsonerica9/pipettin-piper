# Third-party imports
import pprint
import re
import time
import json
import asyncio
from collections import namedtuple
import sys
from copy import deepcopy
import logging

# Internal imports
from .gcode import GcodeBuilder
from . import gcode_primitives as gcodePrimitives

# Custom exceptions
class NotReadyError(Exception):
    """Custom exception to handle unready printer states in try-except clauses.
    """
    pass

class CommanderError(Exception):
    """Custom exception to handle unready printer states in try-except clauses.
    """
    pass

# Custom formatter from: https://stackoverflow.com/questions/384076/how-can-i-color-python-logging-output
class CustomFormatter(logging.Formatter):

    grey = "\x1b[38;20m"
    yellow = "\x1b[33;20m"
    red = "\x1b[31;20m"
    bold_red = "\x1b[31;1m"
    reset = "\x1b[0m"
    format = "%(asctime)s - %(name)s - %(levelname)s - %(message)s (%(filename)s:%(lineno)d)"

    FORMATS = {
        logging.DEBUG: grey + format + reset,
        logging.INFO: format,
        logging.WARNING: yellow + format + reset,
        logging.ERROR: red + format + reset,
        logging.CRITICAL: bold_red + format + reset
    }

    def format(self, record):
        log_fmt = self.FORMATS.get(record.levelno)
        formatter = logging.Formatter(log_fmt)
        return formatter.format(record)


# Argument parsing
DEFAULT_ARGUMENTS = {
    "run_protocol": None,
    "calibration": None,
    "mongo_url": 'mongodb://localhost:27017/',
    "mongo_db": 'pipettin',
    # "gcode_path": "/home/pi/printer_data/gcodes/",
    "gcode_path": "/tmp/",
    "workspace": None,
    "item": None,
    "content": None,
    "move": None,
    "distance": None,
    "command": None,
    "home": None,
    "verbose": True,
    "tool": ""
    }

def make_args(**kwargs):
    """
    Generate default arguments dictionary for parse_args().
    : mongo_url     : URL to the MongoDB server.
    : home          : If set to a valid string, the specified machine's axis homed (i.e. "xyz").
    : calibration   : If set to a valid string, it performs a "callibration" type move (i.e. a "goto" callibration command).
    : run_protocol  : If set to a valid protocol name, it runs a protocol. It must match the name of a protocol in MongoDB.
    : move          : If set to a valid string, it indicates a "move" type command ("jogging" in CNC jargon).
    : command       : If set to a string with a valid GCODE comand, it is passed directly to the machine's controller.
    : distance      : Distance relevant to the command, if any (i.e. for the "move" command).
    : workspace     : Workspace name for the protocol.
    : item          : Name of the relevant platform item in the workspace, if any (i.e. for the "move" or "callibration" commands).
    : content       : Name of the relevant platform content in the workspace, if any (i.e. for the "move" or "callibration" commands).
    : tool          : Name of tool to use.
    : verbose       : If True, a lot of messages will be printed to the console.
    """

    dflt = deepcopy(DEFAULT_ARGUMENTS)

    dflt.update(kwargs)

    return dflt


def parse_args(new_args=None):
    """
    Generates arguments for command_router(). First create defaults and update them with new_args, then convert the
    dict to a named tuple. :param new_args: dictionary with new arguments. :return: a named tuple with updated
    arguments.

    Examples:

        XYZ home:       ["p2g_command", {"home":"xyz","tool":""}]
        Tool home:      ["p2g_command", {"home":"p","tool":"P200"}]
        Run protocol:   ["p2g_command", {"run_protocol":"prueba 2023-01-14T08:40:00.259Z"}]
        Go-to:          ["p2g_command", {"calibration":"goto", "workspace":"calibracion_plataformas", "item":"5x16_1.5_rack 1", "content":"tube1"}]
    """
    
    # Generate an emppty default if None.
    if new_args is None:
        new_args = {}
    else:
        # Fix invalid name for namedtuple conversion
        if "run-protocol" in new_args.keys():
            new_args["run_protocol"] = new_args.pop("run-protocol")

    # Default arguments
    updated_args = make_args(**new_args)

    if updated_args["verbose"]:
        logging.info(f"commander parse_args: parsed arguments: {json.dumps(updated_args, indent=2)}")

    # Convert to named tuple (analogue for "parser.parse_args()"
    args_tuple = namedtuple("Arguments", updated_args.keys())(*updated_args.values())

    return args_tuple


def command_router(gui_command, commander, verbose=False, dry=False):
    """
    This function decides how and what to send to the robot, based on the set of arguments provided by the GUI.
    
    :param gui_command: Arguments from the GUI call.
    :param commander: A pre-instantiated "moonControl" object.
    :param dry: Parse input arguments and return.
    :return: nothing :)
    """

    # Parse GUI command data into a named tuple with full defaults.
    args = parse_args(gui_command)

    if dry:
        commander.logging.info(f"command_router: arguments received: {args}")
        return True

    # Default dwell command
    # dwell_cmd = gcodePrimitives.gcodeDwell(seconds=0.1) # Example: ["G4 P0.1"]

    # Get the verbose argument plainly
    verbose = args.verbose or verbose

    # Parse the arguments, decide what to do, and send commands accordingly.
    # If not a protocol, then keep parsing the options:
    if args.run_protocol is None:

        # Home command
        if args.home is not None:
            pass
            
            # home_sweet_home = []
            # home_tools_cmd = []

            # if verbose:
            #     logging.info(f"command_router message: sending home command: {args.home}")

            # # XYZ axes homing
            # pattern = re.compile(".?[xyz]")
            # if pattern.match(args.home) is not None:
            #     # Enable steppers
            #     # commander.steppers_on()
            #     # Build gcode
            #     home_sweet_home = gcodePrimitives.gcodeHomeXYZ(which_axis=args.home)

            #     # # Send command
            #     # if verbose:
            #     #     logging.info("command_router message: sending 'home' command directly: " + str(home_sweet_home))
            #     # for command in home_sweet_home:
            #     #     await commander.send_gcode_cmd(command, wait=True, check=True, timeout=10.0)
            #     # # Require dwell
            #     # await commander.send_gcode_cmd(dwell_cmd, wait=True, check=True)

            # # Tool homing
            # if args.home.upper().find("P") > -1 or args.home.upper() == "ALL":
            #     if verbose:
            #         logging.info("command_router: Sending pipette axis home...")
            #     # Home
            #     if args.tool != "":
            #         home_tools_cmd = gcodePrimitives.gcodeHomeP(which_axis=args.tool.upper())
            #     else:
            #         home_tools_cmd = gcodePrimitives.gcodeHomeP()

            #     # # Send command
            #     # for command in home_tools_cmd:
            #     #     await commander.send_gcode_cmd(command, wait=True, check=True, timeout=10.0)
            #     # # Require dwell
            #     # await commander.send_gcode_cmd(dwell_cmd, wait=True, check=True)

            # # Return homing commands
            # return home_sweet_home + home_tools_cmd

        # Move command (not a Jog, just a G91 command)
        elif args.move is not None:
            pass
            # # Get move direction from arguments
            # move_direction = str(args.move).lower()

            # # XYZ movements
            # if move_direction in ["x", "y", "z"]:
            #     # Build gcode
            #     move_params = {"x": None, "y": None, "z": None}
            #     move_params[move_direction] = args.distance
            #     move_command = gcodePrimitives.gcodeMove(absolute=False,
            #                                              x=move_params["x"],
            #                                              y=move_params["y"],
            #                                              z=move_params["z"])
            #     # # Send command
            #     # if verbose:
            #     #     logging.info("command_router message: sending 'move' command directly: " + str(move_command))
            #     # for command in move_command:
            #     #     await commander.send_gcode_cmd(command, wait=True, check=True, timeout=10.0)
            #     # # Require dwell
            #     # await commander.send_gcode_cmd(dwell_cmd, wait=True, check=True)

            #     # TODO: Wait for the status report update
            #     # commander.update_sio_wpos(timeout=30)

            #     return move_command

            # # Tool movements
            # elif move_direction == "p":
            #     if verbose:
            #         logging.info(f"command_router message: Pipette move: dist={args.distance}, T={args.tool}")

            #     # Configure pipette if specified
            #     tool_activate_command = []
            #     if args.tool != "":
            #         tool = args.tool.upper()
            #         tool_activate_command = gcodePrimitives.gcodeT(tool)

            #         # # Send command
            #         # for command in tool_activate_command:
            #         #     await commander.send_gcode_cmd(command, wait=True, check=True, timeout=10.0)

            #     # Build move command
            #     move_command = gcodePrimitives.gcodeMove(absolute=False, e=args.distance)

            #     # # Send command
            #     # for command in move_command:
            #     #     await commander.send_gcode_cmd(command, wait=True, check=True, timeout=10.0)
            #     # # Require dwell
            #     # await commander.send_gcode_cmd(dwell_cmd, wait=True, check=True)

            #     return tool_activate_command + move_command

        # Fast command, to be sent directly to the machine.
        elif args.command is not None:
            pass
            # if verbose:
            #     logging.info(f"command_router message: sending other GRBL fast command (non-move): {args.command}")

            # # # Send command directly
            # # await commander.send_gcode_cmd(args.command)

            # return args.command

        # Go-to platform item command
        elif args.calibration == "goto":
            pass
            # if verbose:
            #     logging.info(f"command_router message: sending 'goto' command as fast_grbl_command: {args.calibration}")
            #     logging.info(f"    Using workspace: {args.workspace}")
            #     logging.info(f"    Using item: {args.item}")
            #     logging.info(f"    Over content: {args.content}")
            #     logging.info(f"    With tool: {args.tool}")

            # # Setup database
            # database_tools = MongoObjects(mongo_url=args.mongo_url, 
            #                               database_name=args.mongo_db,
            #                               parser_object=None,  # argparse no longer used
            #                               verbose=args.verbose)

            # # Generate move command
            # # TODO: check if other parts of the code expected a string from "args.content".
            # command, message = database_tools.makeGoToPlatformCommand(workspace_name=args.workspace,
            #                                                           platform_name=args.item,
            #                                                           content_name=args.content["name"],
            #                                                           tool_name=args.tool)

            # # Verbose alert stuff
            # # commander.sio_emit('alert', {"text": message})

            # # # Send command directly to the serial port
            # # if verbose:
            # #     logging.info("command_router message: sending command to serial: " + command)
            # # await commander.send_gcode_cmd(command, wait=True, check=True, timeout=10.0)

            # # Request status update
            # # if verbose:
            # #     logging.info("command_router message: requesting wpos status update for socketio emit: " + command)
            # # time.sleep(0.2)  # Wait for move to start
            # # commander.update_sio_wpos(timeout=10)

            # # Skip disabling steppers
            # # commander.steppers_off()  # Turn off steppers

            # return command

        else:
            if verbose:
                logging.info("command_router message: Nothing done: no protocol, move or home options provided.")

    # Run a protocol
    elif args.run_protocol is not None:
        pass
        # if verbose:
        #     logging.info(f"command_router message: Provided protocol name: '{args.run_protocol}'\n")

        # if verbose:
        #     logging.info("\ncommand_router message: Loading protocol objects from MONGODB.\n")
        # # Setup database
        # database_tools = MongoObjects(mongo_url=args.mongo_url, 
        #                               database_name=args.mongo_db,
        #                               parser_object=None,  # argparse no longer used
        #                               verbose=args.verbose)

        # # Load protocol objects from mongodb:
        # protocol, workspace, platforms_in_workspace = database_tools.getProtocolObjects(protocol_name=args.run_protocol)

        # if verbose:
        #     logging.info("\ncommand_router message: Instantiating protocol task class.\n")
        #     logging.info(json.dumps(protocol, indent=2, default=str))  # https://stackoverflow.com/a/56061155/11524079

        # # Initiate the task class, which will hold all relevant information for the current task
        # builder = GcodeBuilder(protocol, workspace, platforms_in_workspace)

        # # TODO: Pipette configs should be copied
        # # builder.pipettes = commander.pipette.pipettes

        # # Generate GCODE for the task, it is saved in the task class object
        # builder.parseProtocol()

        # # Print it
        # if verbose:
        #     logging.info(pprint.pformat(builder.commands))

        # # Get the command list, and pass it on to the commander.
        # # if not args.dry:
        # #     await commander.run_gcode_protocol(commands=builder.commands)
        
        # # Save GCODE to a file in Klipper's printer_data
        # if verbose:
        #     logging.info("\ncommand_router message: Saving protocol\n")
        # commander.save_gcode_protocol(gcode_commands=builder.commands, 
        #                               file=f"{args.gcode_path}/{args.run_protocol}.gcode")

        # # Since coroutines_moon will execute any returned GCODE commands,
        # # if we want to run the GCODE manually we must return an empty list.
        # return []
        # # return builder.commands

    else:
        msg = f"ncommand_router: The call to commander.py couldn't route the command to an existing condition."
        msg += f" Got gui_command={pprint.pformat(gui_command)}"
        logging.error(msg)
        raise Exception(msg)

    logging.info("\ncommand_router message: done!\n")

    return 0
