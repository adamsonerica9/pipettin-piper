import os
import signal
import socket
import subprocess
import time


klippy_config = {
    "klippy": os.path.expanduser("~/klipper/klippy/klippy.py"),
    "cfg_file": os.path.expanduser("~/klipper/config/configs-pipetting-bot/config-pi-pico/printer-multi_extruder.cfg"),
    "log_file": "/tmp/klippy.log", 
    "klippy_uds": "/tmp/klippy_uds"
    }

class klippy_process(object):
    def __init__(self, klippy, cfg_file, log_file, klippy_uds):
        
        # self.kcommand = f"python3 {klippy} {cfg_file} -l {log_file} -a {klippy_uds}"
        self.kcommand  = ["python3", klippy, cfg_file, "-l", log_file, "-a", klippy_uds]
        
        hostname = socket.gethostname()
        if hostname == "archer":
            #klipper = subprocess.run(args=["python3", klippy, cfg_file, "-l", log_file, "-a", klippy_uds],
            #                         #shell=True, 
            #                         #check=True,
            #                         capture_output=True)
            self.klipper = subprocess.Popen(args=self.kcommand,
                                            stdout=subprocess.PIPE,
                                            stderr=subprocess.PIPE,
                                            preexec_fn=os.setsid
                                            )
            
            self.pid = os.getpgid(self.klipper.pid)
            print(f"Started klipper with command:\n    {' '.join(self.kcommand)}")
            print(f"Logging to file:\n    {log_file}")
            
    def terminate(self):
        self.klipper.terminate()
        
    def sigterm(self):
        os.killpg(self.pid, signal.SIGTERM)
    
    def kill(self):
        self.klipper.kill()
    
    def returncode(self):
        return self.klipper.returncode

    def stop(self):
        self.terminate()
        time.sleep(0.2)
        self.kill()
        return self.returncode()
